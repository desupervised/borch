# This CITATION.cff file was generated with cffinit.
# Visit https://bit.ly/cffinit to generate yours today!

cff-version: 1.2.0
title: >-
  Borch: A Deep Universal Probabilistic Programming
  Language
message: >-
  If you use this software, please cite it using the
  metadata from this file.
type: software
authors:
  - given-names: Lewis
    family-names: Belcher
    email: lb@desupervised.io
    affiliation: Desupervised
    orcid: 'https://orcid.org/0000-0001-9680-078X'
  - given-names: Michael
    family-names: Green
    email: mg@desupervised.io
    affiliation: Desupervised
    orcid: 'https://orcid.org/0000-0003-1507-1613'
  - given-names: Johan
    family-names: Gudmundsson
    email: jg@desupervised.io
    affiliation: Desupervised
    orcid: 'https://orcid.org/0000-0002-7316-0334'
identifiers:
  - type: doi
    value: 10.48550/arXiv.2209.06168
    description: The ArXiv deposit of the encompassing paper.
  - type: url
    value: 'https://borch.readthedocs.io'
    description: Documentation of the software package.
repository-code: 'https://gitlab.com/desupervised/borch'
abstract: >-
  Borch is a scalable and flexible deep universal
  probabilistic programming language built on top of
  PyTorch. It enables deep learning practitioners to
  swiftly and easily enable uncertainty
  quantification in all predictions for all types of
  deep neural network architectures.
keywords:
  - borch
  - deep learning
  - probabilistic programming
  - deep universal probabilistic programming
  - pytorch
  - bayesian
license: Apache-2.0
version: 0.2.0
date-released: '2022-09-14'

