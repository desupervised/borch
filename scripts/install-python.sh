# Install Python

set -e

apt install -yq software-properties-common
add-apt-repository -y ppa:deadsnakes/ppa
apt install -yq python3.7-dev python3.7-distutils
ln -sf /usr/bin/python3.7 /usr/bin/python
curl https://bootstrap.pypa.io/get-pip.py | python
