"""
Utilities to help with running tutorials.
"""

from importlib import import_module
from inspect import getmembers
import re
import sys

from borch.utils import testing

# Must prepend "" to sys.path for relative importing within tutorials
if "" not in sys.path:
    sys.path.insert(0, "")


def _is_tutorial_test(obj):
    try:
        return issubclass(obj, testing.ScriptTest)
    except TypeError:
        return False


def run_before_scripts():
    """Run all `before_script`s from all tutorial tests."""
    paths = (
        f
        for f in testing.get_python_files(["tutorials"])
        if testing.TEST_FILE_RE.search(f)
    )
    for path in paths:
        mod = import_module(testing.filepath_to_import_path(path))
        for name, cls in getmembers(mod, _is_tutorial_test):
            cls.before_script()


if __name__ == "__main__":
    run_before_scripts()
