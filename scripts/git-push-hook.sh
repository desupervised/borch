
# todo: once gitlab supports push flags use this instead of littering commit messages

# try to find [ci skip] or [skip ci] in last commit message
git log -1 --pretty=%B | grep -Pq '\[ci skip\]|\[skip ci\]' ; skipping=$?

# if not found (ci will build), then run linting and tests before pushing
if [ $skipping != 0 ] ; then
  echo "#######################################################################"
  echo "CI is set to build. Running make tests pre-push..."
  echo "To skip add [ci skip] to last commit message using 'git commit --amend'"
  echo "#######################################################################"
  echo

  # check for uncommited changes
  git status | grep -Pq '\smodified:\s' ; uncommited=$?
  if [ $uncommited = 0 ] ; then
    echo "ERROR: Found uncommited changes in your path. Please stash or commit"
    echo "       these before continuing."
    exit 1
  fi

  # exit on non-zero status
  set -e

  cd `git rev-parse --show-toplevel`

   make lint test-fast

  echo -e "\nCongrats on your passing code :)\n"
fi


