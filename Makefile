SHELL = /bin/bash
PACKAGE_DIR = src/borch
TESTS_DIR =  tests
TUTORIALS_DIR = tutorials
PYLINT_TESTS_DISABLE = "missing-docstring,protected-access,not-callable,abstract-method,redefined-outer-name,expression-not-assigned,pointless-statement"


##############################################################################
# Install
##############################################################################

.PHONY: install
## Install the package
install:
	pip install  .


.PHONY: install-dev
## Install the package in "development"/"editable" mode
install-dev:
	# DEVELOP=1 pip install -e .[docs,lint,test${BORCH_EXTRAS}]
	DEVELOP=1 pip install -e .[lint,test${BORCH_EXTRAS}]


##############################################################################
# Linting
##############################################################################

.PHONY: lint-source
## Run linting on the source code (not including tests)
lint-source:
	ruff check $(PACKAGE_DIR)
	# black --check $(PACKAGE_DIR)
	# pylint $(PACKAGE_DIR)


.PHONY: lint-tests
## Run linting on the test code
lint-tests:
	ruff check $(TESTS_DIR)
	# black $(TESTS_DIR) --check
	# pylint $(TESTS_DIR) \
	#     --ignore-patterns= \
	#     --disable=$(PYLINT_TESTS_DISABLE) \
	#     --method-rgx='(([a-z][a-z0-9_]{2,70})|(_[a-z0-9_]*)|setUpClass|tearDownClass|setUp|tearDown)$$' \
	#     --max-public-methods=50 \
	#     --class-rgx='[A-Z_][a-zA-Z0-9_]+$$' \
	#     --function-rgx='(([a-z][a-z0-9_]{2,70})|(_[a-z0-9_]*)|setUpModule|tearDownModule)$$'


.PHONY: lint
## Run linting on all code
lint: lint-source lint-tests


##############################################################################
# Testing
##############################################################################

.PHONY: test
## Run all tests and coverage
test:
	pytest ${TEST_ARGS} --doctest-modules --doctest-continue-on-failure --cov=$(PACKAGE_DIR) --disable-warnings $(PACKAGE_DIR) $(TESTS_DIR)
	# pytest ${TEST_ARGS} --cov=$(PACKAGE_DIR) --disable-warnings $(PACKAGE_DIR) $(TESTS_DIR)

.PHONY: test-fast
## Run all fast tests where the coverage is checked
test-fast:
	TEST_ARGS='${TEST_ARGS} -m "not (integration or slow or predefined or tutorial)"' make test


.PHONY: test-tutorials
## Run tests on all tutorial
test-tutorials:
	pytest ${TEST_ARGS} $(TUTORIALS_DIR)


##############################################################################
# Others (convenient targets for formatting, push-hook, etc.)
##############################################################################


.PHONY: format
## Auto-format entire python project
format:
	ruff check $(PACKAGE_DIR) $(TESTS_DIR) --fix-only
	# isort -rc --atomic $(PACKAGE_DIR)
	# black $(PACKAGE_DIR) *.py
	# black $(TESTS_DIR) *.py


.PHONY: push-hook
## Add git push hook link to the relevant location
push-hook:
	@# NB this assumes that it is run from the base borch directory
	@if [[ -d .git ]] ; then \
		mkdir -p .git/hooks && \
	  ln -sf ../../scripts/git-push-hook.sh .git/hooks/pre-push \
	; fi


##############################################################################
# Help
##############################################################################

# COLORS
GREEN  := $(shell tput -Txterm setaf 2)
YELLOW := $(shell tput -Txterm setaf 3)
RESET  := $(shell tput -Txterm sgr0)
TARGET_MAX_CHAR_NUM=20


.PHONY: help
## Show help
help:
	@echo ''
	@echo 'Usage:'
	@echo '  ${YELLOW}make ${GREEN}<target>${RESET}'
	@echo ''
	@echo 'Targets:'
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  ${YELLOW}%-$(TARGET_MAX_CHAR_NUM)s ${GREEN}%s${RESET}\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)
