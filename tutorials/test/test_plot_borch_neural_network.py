from os.path import abspath
from unittest import TestCase

import pytest

from borch import Module
from borch.utils.testing import ScriptTest


@pytest.mark.tutorial
class TestPlotBorchNeuralNetwork(ScriptTest, TestCase):
    path = abspath(f"{__file__}/../../plot_borch_neural_network.py")

    def test_net_is_module_instance(self):
        self.assertIsInstance(self.globals["net"], Module)
