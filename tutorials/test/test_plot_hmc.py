from os.path import abspath
from unittest import TestCase

import pytest

from borch import Module
from borch.utils.testing import ScriptTest


@pytest.mark.tutorial
class TestPlotHMC(ScriptTest, TestCase):
    mock_imports = ("matplotlib", "matplotlib.pyplot")
    path = abspath(f"{__file__}/../../plot_hamiltonian_monte_carlo.py")

    def test_net_is_module_instance(self):
        self.assertIsInstance(self.globals["samples_nuts"], dict)
