from os.path import abspath
from unittest import TestCase

import pytest

from borch import RandomVariable
from borch.utils.testing import ScriptTest


@pytest.mark.tutorial
class TestPlotBorchPrimitives(ScriptTest, TestCase):
    mock_imports = ("matplotlib", "matplotlib.pyplot")
    path = abspath(f"{__file__}/../../plot_borch_primitives.py")

    def test_rvar_is_random_variable_instance(self):
        self.assertIsInstance(self.globals["rvar"], RandomVariable)
