from os.path import abspath
from unittest import TestCase

import pytest

from borch import Module
from borch.utils.testing import ScriptTest


@pytest.mark.tutorial
class TestPlotPriorsForNN(ScriptTest, TestCase):
    mock_imports = ("matplotlib", "matplotlib.pyplot")
    path = abspath(f"{__file__}/../../plot_priors_for_nn.py")

    def test(self):
        1 - 1

    def test_net_is_module_instance(self):
        self.assertIsInstance(self.globals["net"], Module)
