.. automodule:: borch
    :imported-members:
    :members:
    :show-inheritance:

.. toctree::
   :maxdepth: 2
   borch.distributions
   borch.infer
   borch.metrics
   borch.nn
   borch.utils
   borch.posterior
   borch.module
   borch.optimizer_collection
   borch.rv_factories
   borch.version
