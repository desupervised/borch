Welcome to borch's documentation!
=================================

For development and installation posterior lines please read trough
the project information links. It covers installation procedure,
development workflow etc.


For examples of how to use borch see the Tutorials section.



.. toctree::
   :glob:
   :maxdepth: 2
   :caption: Project Information

   notes/README.md
   notes/CONTRIBUTING.md
   notes/CODE_OF_CONDUCT.md
   tutorials/index.rst

.. toctree::
   :maxdepth: 2
   :caption: Package Reference

   borch
   nn
   infer
   posterior
   metrics
   utils.func_tools
   utils.inference_conversion
   utils.init
   utils.module_utils
   utils.namespace_tools
   utils.numpy_utils
   utils.state_dict
   utils.torch_utils
   utils.random_variable_utils


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

