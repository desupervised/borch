FROM python:3.12

ARG DEBIAN_FRONTEND=noninteractive

RUN apt update -q && \
    apt install -yq --no-install-recommends \
      curl \
      git  && \
    pip install torch --index-url https://download.pytorch.org/whl/cpu
WORKDIR /borch

ENV LC_ALL="C.UTF-8" \
    LANG="C.UTF-8"

COPY . .
RUN  make install

ENTRYPOINT ["/bin/bash"]
