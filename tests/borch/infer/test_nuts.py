"""tests for Nuts file."""

import unittest

import numpy as np
import torch

from borch.infer.nuts import dual_averaging, find_reasonable_epsilon, nuts_step
from borch.utils.torch_utils import get_device, seed

DEVICE = get_device()


class Test_find_reasonable_epsilon(unittest.TestCase):
    def setUp(self):
        seed(1)

    def test_find_reasonable_epsilon(self):
        par = [torch.randn(2, requires_grad=True, device=DEVICE) for _ in range(3)]

        def closure():
            return sum(
                -(torch.ones(2, device=DEVICE) - pp).pow(2).sum() / par[0].pow(2).sum()
                for pp in par
            )

        epsilon, epsilon_bar, h_bar = find_reasonable_epsilon(par, closure)
        assert isinstance(epsilon, float)
        assert isinstance(epsilon_bar, float)
        assert isinstance(h_bar, float)

    def reasonable_epsilon_no_gradients_for_param(self):
        par = [torch.randn(2, requires_grad=False, device=DEVICE) for _ in range(3)]

        def closure():
            return sum(-(pp).pow(2).sum() for pp in par)

        epsilon, epsilon_bar, h_bar = find_reasonable_epsilon(par, closure)
        assert isinstance(epsilon, float)
        assert isinstance(epsilon_bar, float)
        assert isinstance(h_bar, float)


class Test_nuts_step(unittest.TestCase):
    def setUp(self):
        seed(1)

    def test_nuts_step(self):
        par = [torch.randn(2, requires_grad=True, device=DEVICE) for _ in range(3)]

        def closure():
            return -sum(-(pp).pow(2).sum() for pp in par)

        accept_prob = nuts_step(epsilon=0.1, parameters=par, closure=closure)
        assert isinstance(accept_prob, float)

    def test_mean_sd_normal(self):
        par = [torch.randn(1, requires_grad=True, device=DEVICE)]

        def closure():
            return 0.5 * (par[0]).pow(2).sum()

        par_val = []
        for _ in range(500):
            nuts_step(epsilon=2, parameters=par, closure=closure)
            par_val.append(par[0].item())

        self.assertAlmostEqual(np.mean(par_val), 0.0, delta=0.2)
        self.assertAlmostEqual(np.std(par_val), 1.0, delta=0.2)

    def test_hit_max_tree_depth(self):
        par = [torch.randn(2, requires_grad=True, device=DEVICE) for _ in range(3)]

        def closure():
            return -sum(-(pp).pow(2).sum() for pp in par)

        accept_prob = nuts_step(
            epsilon=0.1, parameters=par, closure=closure, max_tree_depth=1,
        )
        assert isinstance(accept_prob, float)


class Test_dual_averaging(unittest.TestCase):
    def test_dual_averaging(self):
        epsilon, epsilon_bar, h_bar = 1, 1, 0
        epsilon_new, epsilon_bar_new, h_bar_new = dual_averaging(
            0.5, 1, epsilon, epsilon_bar, h_bar,
        )
        assert isinstance(epsilon_new, float)
        assert isinstance(epsilon_bar_new, float)
        assert isinstance(h_bar_new, float)

        assert epsilon_new > epsilon
        assert epsilon_bar_new > epsilon_bar


if __name__ == "__main__":
    unittest.main()
