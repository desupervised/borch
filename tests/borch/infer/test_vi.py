"""tests for vi file."""
import unittest

import pytest
import torch
from torch import optim

from borch import distributions as dist
from borch.infer.vi import (
    analytical_kl_divergence_loss,
    elbo_entropy_loss,
    elbo_loss,
    elbo_path_derivative_loss,
    elbo_rb_score_function,
    elbo_score_entropy,
    elbo_score_function,
    elbo_score_function_loss,
    prior_loss,
    vi_analytical_kl_loss,
    vi_loss,
    vi_regularization,
)
from borch.utils.torch_utils import get_device

DEVICE = get_device()


def normal():
    return dist.Normal(torch.zeros(1, device=DEVICE), torch.ones(1, device=DEVICE))


def cauchy():
    return dist.Cauchy(torch.zeros(1, device=DEVICE), torch.ones(1, device=DEVICE))


class Test_elbo_rb_score_function(unittest.TestCase):
    def setUp(self):
        self.mu = torch.ones(1, requires_grad=True)
        self.sigma = torch.ones(1, requires_grad=True)

    def sub_sample(self, n_var):
        return {
            "p_dists": [dist.Normal(1, 1) for _ in range(n_var)],
            "q_dists": [dist.Normal(self.mu, self.sigma) for _ in range(n_var)],
            "values": [self.mu + self.sigma * torch.randn(1) for _ in range(n_var)],
            "parameters": [self.mu, self.sigma],
            "regularization": torch.ones(1),
        }

    def sub_sampels(self, n_sampels):
        return [self.sub_sample(5) for _ in range(n_sampels)]

    def test_same_sub_samples(self):
        sub_sampels = [
            {
                "p_dists": [dist.Normal(1, 1) for _ in range(5)],
                "q_dists": [dist.Normal(self.mu, self.sigma) for _ in range(5)],
                "values": [self.mu + self.sigma for _ in range(5)],
                "parameters": [self.mu, self.sigma],
                "regularization": torch.ones(1),
            }
            for _ in range(5)
        ]

        self.assertRaises(ValueError, lambda: elbo_rb_score_function(sub_sampels))

    def test_is_tensor(self):
        assert isinstance(elbo_rb_score_function(self.sub_sampels(5)), torch.Tensor)

    def test_shape(self):
        assert elbo_rb_score_function(self.sub_sampels(5)).size() == torch.Size([])

    def test_tensors_as_dist_args(self):
        n_var = 5
        mu = torch.ones(1, requires_grad=True, device=DEVICE)
        sigma = torch.ones(1, requires_grad=True, device=DEVICE)

        sub_sample = [
            {
                "p_dists": [normal() for _ in range(n_var)],
                "q_dists": [dist.Normal(mu, sigma) for _ in range(n_var)],
                "values": [
                    mu + sigma * torch.randn(1, device=DEVICE) for _ in range(n_var)
                ],
                "parameters": [mu, sigma],
                "regularization": torch.ones(1, device=DEVICE),
            }
            for _ in range(5)
        ]

        assert isinstance(elbo_rb_score_function(sub_sample), torch.Tensor)


class Test_prior_loss(unittest.TestCase):
    def test_normal(self):
        self.assertAlmostEqual(
            prior_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)),
            1.4189,
            delta=1e-4,
        )

    def test_gamma(self):
        self.assertAlmostEqual(
            elbo_loss(dist.Gamma(1, 1), dist.Normal(0, 1), 0.5 * torch.ones(1)),
            -0.5439,
            delta=0.1,
        )


class Test_elbo_loss(unittest.TestCase):
    def test_elbo(self):
        assert elbo_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)) == 0
        self.assertAlmostEqual(
            elbo_loss(dist.Cauchy(0, 1), dist.Normal(0, 1), torch.ones(1)),
            0.4189,
            delta=0.1,
        )

    def test_tensors_as_dist_args(self):
        assert elbo_loss(normal(), normal(), torch.ones(1, device=DEVICE)) == 0

    def test_delta_as_qdist(self):
        par = torch.ones(1, requires_grad=True)
        q_dist = dist.Delta(par)
        elbo_loss(dist.Normal(0, 1), q_dist, q_dist.rsample()).backward()
        assert float(par.grad.sum()) == 1


class Test_elbo_score_function_loss(unittest.TestCase):
    def test_elbo_score_function_loss(self):
        assert elbo_score_function_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)) == 0
        self.assertAlmostEqual(
            elbo_score_function_loss(
                dist.Cauchy(0, 1), dist.Normal(0, 1), torch.zeros(1),
            ),
            -0.2075,
            delta=0.1,
        )

    def test_tensors_as_dist_args(self):
        assert elbo_score_function_loss(normal(), normal(), torch.ones(1, device=DEVICE)) == 0

    def test_delta_as_qdist_raises_error(self):
        par = torch.ones(1, requires_grad=True)
        q_dist = dist.Delta(par)
        with pytest.raises(RuntimeError):
            elbo_score_function_loss(
                dist.Normal(0, 1), q_dist, q_dist.rsample(),
            ).backward()


class Test_elbo_path_derivative_loss(unittest.TestCase):
    def test_elbo_path_derivative_loss(self):
        assert elbo_path_derivative_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)) == 0
        self.assertAlmostEqual(
            elbo_path_derivative_loss(
                dist.Cauchy(0, 1), dist.Normal(0, 1), torch.ones(1),
            ),
            0.4189,
            delta=0.1,
        )

    def test_tensors_as_dist_args(self):
        assert elbo_path_derivative_loss(normal(), normal(), torch.ones(1, device=DEVICE)) == 0


class Test_elbo_entropy_loss(unittest.TestCase):
    def test_elbo_entropy_loss(self):
        assert elbo_entropy_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)) == 0
        self.assertAlmostEqual(
            elbo_entropy_loss(dist.Cauchy(0, 1), dist.Normal(0, 1), torch.ones(1)),
            0.4189,
            delta=0.1,
        )

    def test_tensors_as_dist_args(self):
        assert elbo_entropy_loss(normal(), normal(), torch.ones(1, device=DEVICE)) == 0


class Test_analytical_kl_divergence_loss(unittest.TestCase):
    def test_analytical_kl_divergence_loss(self):
        assert analytical_kl_divergence_loss(dist.Normal(0, 1), dist.Normal(0, 1), torch.ones(1)) == 0
        assert analytical_kl_divergence_loss(dist.Normal(0, 1), dist.Normal(1, 1), torch.ones(1)) == 0.5

        self.assertAlmostEqual(
            analytical_kl_divergence_loss(
                dist.Cauchy(1, 1), dist.Normal(0, 1), torch.ones(1),
            ),
            -0.2742,
            delta=0.1,
        )

    def test_tensors_as_dist_args(self):
        assert analytical_kl_divergence_loss(normal(), normal(), torch.ones(1, device=DEVICE)) == 0


class Test_vi_regularization(unittest.TestCase):
    def test_different_input_length(self):
        with pytest.raises(ValueError):
            vi_regularization(
                [dist.Normal(0, 1) for _ in range(10)],
                [dist.Normal(0, 1) for _ in range(5)],
                [torch.ones(1) for _ in range(5)],
                elbo_loss,
            )

    def test_size(self):
        assert vi_regularization([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], elbo_loss).size() == torch.Size([])

    def test_elbo_loss(self):
        assert vi_regularization([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], elbo_loss) == 0

        self.assertAlmostEqual(
            vi_regularization(
                [dist.Cauchy(0, 1) for _ in range(1)],
                [dist.Normal(0, 1) for _ in range(1)],
                [torch.ones(1) for _ in range(1)],
                elbo_loss,
            ),
            0.4189,
            delta=0.01,
        )

    def test_elbo_path_derivative_loss(self):
        self.assertAlmostEqual(
            vi_regularization(
                [dist.Cauchy(0, 1) for _ in range(1)],
                [dist.Normal(0, 1) for _ in range(1)],
                [torch.ones(1) for _ in range(1)],
                elbo_path_derivative_loss,
            ),
            0.4189,
            delta=0.01,
        )

    def test_tensors_as_dist_args(self):
        self.assertAlmostEqual(
            vi_regularization(
                [
                    dist.Cauchy(
                        torch.zeros(1, device=DEVICE), torch.ones(1, device=DEVICE),
                    )
                    for _ in range(5)
                ],
                [normal() for _ in range(5)],
                [torch.ones(1, device=DEVICE) for _ in range(5)],
                elbo_loss,
            ),
            2.0947,
            delta=0.01,
        )

    def test_some_delta_dist_as_q_dist(self):
        self.assertAlmostEqual(
            vi_regularization(
                [dist.Normal(0, 1) for _ in range(5)],
                [dist.Normal(0, 1) for _ in range(2)]
                + [dist.Delta(1) for _ in range(3)],
                [torch.ones(1) for _ in range(5)],
                elbo_loss,
            ),
            4.2568,
            delta=1e-4,
        )

        self.assertAlmostEqual(
            vi_regularization(
                [dist.Cauchy(0, 1) for _ in range(1)],
                [dist.Delta(1) for _ in range(1)],
                [torch.ones(1) for _ in range(1)],
                elbo_loss,
            ),
            1.8379,
            delta=1e-4,
        )


class Test_elbo_score_function(unittest.TestCase):
    def test_different_input_length(self):
        with pytest.raises(ValueError):
            elbo_score_function(
                [dist.Normal(0, 1) for _ in range(10)],
                [dist.Normal(0, 1) for _ in range(5)],
                [torch.ones(1) for _ in range(5)],
                elbo_loss,
            )

    def test_size(self):
        assert elbo_score_function([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], torch.ones(1)).size() == torch.Size([])

    def test_elbo_loss(self):
        assert elbo_score_function([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], torch.zeros(1)) == 0

    def test_tensors_as_dist_args(self):
        self.assertAlmostEqual(
            elbo_score_function(
                [cauchy() for _ in range(5)],
                [normal() for _ in range(5)],
                [torch.ones(1, device=DEVICE) for _ in range(5)],
                torch.zeros(1, device=DEVICE),
            ),
            -14.8612,
            delta=0.01,
        )


class Test_elbo_score_entropy(unittest.TestCase):
    def test_different_input_length(self):
        with pytest.raises(ValueError):
            elbo_score_entropy(
                [dist.Normal(0, 1) for _ in range(10)],
                [dist.Normal(0, 1) for _ in range(5)],
                [torch.ones(1) for _ in range(5)],
                elbo_loss,
            )

    def test_size(self):
        assert elbo_score_entropy([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], torch.ones(1)).size() == torch.Size([])

    def test_elbo_loss(self):
        self.assertAlmostEqual(
            float(
                elbo_score_entropy(
                    [dist.Normal(0, 1) for _ in range(5)],
                    [dist.Normal(0, 1) for _ in range(5)],
                    [torch.ones(1) for _ in range(5)],
                    torch.zeros(1),
                ),
            ),
            -57.4294,
            delta=0.001,
        )

    def test_tensors_as_dist_args(self):
        self.assertAlmostEqual(
            elbo_score_entropy(
                [cauchy() for _ in range(5)],
                [normal() for _ in range(5)],
                [torch.ones(1, device=DEVICE) for _ in range(5)],
                torch.zeros(1, device=DEVICE),
            ),
            -72.2906,
            delta=0.01,
        )


class Test_vi_loss(unittest.TestCase):
    def setUp(self):
        self.loss_fn = vi_loss

    def test_different_input_length(self):
        with pytest.raises(ValueError):
            self.loss_fn(
                [dist.Normal(0, 1) for _ in range(10)],
                [dist.Normal(0, 1) for _ in range(5)],
                [torch.ones(1) for _ in range(5)],
                [False for _ in range(10)],
            )

    def test_size(self):
        assert vi_loss([dist.Normal(0, 1) for _ in range(5)], [dist.Normal(0, 1) for _ in range(5)], [torch.ones(1) for _ in range(5)], [False for _ in range(5)]).size() == torch.Size([])

    def test_tensors_as_dist_args(self):
        assert self.loss_fn([normal() for _ in range(5)], [normal() for _ in range(5)], [torch.ones(1, device=DEVICE) for _ in range(5)], [False for _ in range(5)]) == 0

    def test_with_rsample_and_kl_support(self):
        p_dists = [dist.Gamma(0.1, 1), dist.Beta(1, 1), dist.Bernoulli(0.1)]
        q_dists = [dist.Normal(0, 1), dist.Gamma(1, 1), dist.Bernoulli(0.1)]
        values = [torch.randn(1).abs() for _ in range(3)]
        observed = [False for _ in range(3)]
        dist.kl_divergence(dist.Normal(0, 1), dist.Normal(0, 1))
        assert isinstance(self.loss_fn(p_dists, q_dists, values, observed), torch.Tensor)

    def test_with_no_rsample_and_kl_support_for_one_of_them(self):
        q_dists = [
            dist.Gamma(0.1, 1),
            dist.Beta(1, 1),
            dist.Categorical(torch.tensor([0.25, 0.25, 0.25, 0.25])),
        ]
        p_dists = [dist.Normal(0, 1), dist.Gamma(1, 1), dist.Bernoulli(0.1)]
        values = [torch.ones(1) for _ in range(3)]
        observed = [False for _ in range(3)]
        assert isinstance(self.loss_fn(p_dists, q_dists, values, observed), torch.Tensor)

    def test_only_observed(self):
        loss = self.loss_fn(
            [dist.Normal(0, 1) for _ in range(10)],
            [dist.Normal(0, 1) for _ in range(5)],
            [torch.ones(1) for _ in range(5)],
            [True for _ in range(10)],
        )
        self.assertAlmostEqual(loss, 7.0947, delta=0.1)

    def test_linear_regression_delta_q_dist(self):
        par = torch.zeros(1, requires_grad=True)
        x = torch.randn(1000)
        w_true = 1
        observed = [False, True]
        opt = optim.LBFGS([par])
        for _ in range(10):

            def closure():
                opt.zero_grad()
                delta = dist.Delta(par)
                sample = delta.rsample()
                q_dists = [delta, dist.Normal(sample * x, 1)]
                p_dists = [dist.Normal(0, 1), dist.Normal(sample * x, 1)]
                values = [sample, w_true * x]

                loss = self.loss_fn(p_dists, q_dists, values, observed)
                loss.backward()
                return loss

            opt.step(closure)
        self.assertAlmostEqual(float(par), float(w_true), delta=1e-2)


class Test_vi_analytical_kl_loss(Test_vi_loss):
    def setUp(self):
        self.loss_fn = vi_analytical_kl_loss
