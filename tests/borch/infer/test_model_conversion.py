"""tests."""

import unittest

import torch

import borch.distributions as dist
from borch.infer.model_conversion import model_to_neg_log_prob_closure
from borch.module import Module
from borch.posterior.pointmass import PointMass


class Test_model_to_neg_log_prob_closure(unittest.TestCase):
    def test_model_to_neg_log_prob_closure(self):
        def forward(latent):
            latent.weight1 = dist.Gamma(0.5, 0.5 / 2)
            latent.weight2 = dist.Normal(loc=1, scale=2)

        model = Module()

        def model_call():
            forward(model)

        trace = PointMass()
        model.posterior = trace
        closure = model_to_neg_log_prob_closure(model_call, model)
        assert isinstance(closure(), torch.Tensor)


if __name__ == "__main__":
    unittest.main()
