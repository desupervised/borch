import unittest

import numpy as np
import torch

from borch.infer.hmc import _all_nan_tensor_to_zero, hmc_step, leapfrog
from borch.utils.torch_utils import detach_copy_tensors, get_device, seed

DEVICE = get_device()
NAN = float("nan")


class Test_all_nan_tensor_to_zero(unittest.TestCase):
    def test_returns_same_tensor_when_one_element_is_real(self):
        tensor = torch.tensor([NAN, NAN, 1.0])
        converted = _all_nan_tensor_to_zero(tensor)
        assert id(tensor) == id(converted)

    def test_returns_new_tensor_when_all_nan(self):
        tensor = torch.tensor([NAN, NAN])
        converted = _all_nan_tensor_to_zero(tensor)
        assert id(tensor) != id(converted)

    @staticmethod
    def test_returns_all_zeros_when_input_is_all_nan():
        tensor = torch.tensor([NAN, NAN])
        converted = _all_nan_tensor_to_zero(tensor)
        np.testing.assert_equal(torch.zeros_like(tensor).numpy(), converted.numpy())

    def test_basic_numeric(self):
        converted = _all_nan_tensor_to_zero(0)
        assert converted == 0

    def test_just_nan(self):
        converted = _all_nan_tensor_to_zero(NAN)
        assert converted == 0


class Test_leapfrog(unittest.TestCase):
    def setUp(self):
        seed(1)

    def test_leapfrog(self):
        par = [torch.randn(2, requires_grad=True, device=DEVICE) for _ in range(3)]

        def closure():
            return sum(-(pp).pow(2).sum() for pp in par)

        theta = detach_copy_tensors(par)
        r_values = [torch.randn(rvar.shape, device=DEVICE) for rvar in theta]

        theta_tilde, r_tilde = leapfrog(theta, r_values, 0.1, par, closure)

        assert len(theta), len(theta_tilde)
        assert len(r_values), len(r_tilde)

        for val in theta_tilde:
            assert isinstance(val, torch.Tensor)

        for val in r_tilde:
            assert isinstance(val, torch.Tensor)


class Test_hmc_step(unittest.TestCase):
    def setUp(self):
        seed(1)

    def test_hmc_step(self):
        par = [torch.randn(2, requires_grad=True, device=DEVICE) for _ in range(3)]

        def closure():
            return sum(-(pp).pow(2).sum() for pp in par)

        accept_prob = hmc_step(epsilon=0.1, L=5, parameters=par, closure=closure)
        assert isinstance(accept_prob, float)

    def test_mean_sd_normal(self):
        par = [torch.randn(1, requires_grad=True)]

        def closure():
            return 0.5 * (par[0]).pow(2).sum()

        par_val = []
        for _ in range(100):
            hmc_step(epsilon=0.5, L=3, parameters=par, closure=closure)
            par_val.append(par[0].item())

        self.assertAlmostEqual(np.mean(par_val), 0.0, delta=0.2)
        self.assertAlmostEqual(np.std(par_val), 1.0, delta=0.2)


if __name__ == "__main__":
    unittest.main()
