"""tests for log_joint file."""
import unittest

import torch

import borch.distributions as dist
from borch.infer import log_prob
from borch.infer.log_prob import (
    log_prob_volume_adjustment,
    negative_log_prob,
    negative_log_prob_loss,
    volume_adjustment,
)
from borch.utils.torch_utils import get_device

DEVICE = get_device()


def normal():
    return dist.Normal(torch.ones(1, device=DEVICE), torch.ones(1, device=DEVICE))


class Test_log_prob_volume_adjustment(unittest.TestCase):
    def test_log_prob_volume_adjustment(self):
        assert log_prob_volume_adjustment([dist.Normal(0, 1)], [torch.ones(1)]) == torch.zeros(1)
        assert log_prob_volume_adjustment([dist.Gamma(0.1, 1)], [torch.ones(1) * 0.001]) != torch.zeros(1)
        assert isinstance(log_prob_volume_adjustment([dist.Gamma(0.1, 1)], [torch.ones(1)]), torch.Tensor)

    def test_tensor_as_dist_args(self):
        assert log_prob_volume_adjustment([normal()], [torch.ones(1, device=DEVICE)]) == torch.zeros(1, device=DEVICE)


class Test_volume_adjustment(unittest.TestCase):
    def test_volume_adjustment_normal(self):
        assert volume_adjustment(dist.Normal(1, 1 / 2), torch.ones(1)) == 0

    def test_volume_adjustment_gamma(self):
        assert volume_adjustment(dist.Gamma(1, 1 / 2), torch.ones(1)) == 0
        self.assertAlmostEqual(
            float(volume_adjustment(dist.Gamma(1, 1 / 2), 0.1 * torch.ones(1))),
            -2.3026,
            delta=0.01,
        )

    def test_tesor_as_dist_args(self):
        assert volume_adjustment(dist.Gamma(torch.ones(1, device=DEVICE), torch.ones(1, device=DEVICE)), torch.ones(1, device=DEVICE)) == 0

    def test_scalar_value(self):
        assert volume_adjustment(dist.Normal(0, 1 / 2), torch.ones(1).sum()) == 0


class Test_negative_log_prob(unittest.TestCase):
    def test_log_prob(self):
        assert isinstance(negative_log_prob([dist.Normal(1, 1 / 2) for _ in range(5)], [torch.randn(1) for _ in range(5)]), torch.Tensor)
        assert negative_log_prob([], []) == 0
        assert negative_log_prob([dist.Normal(1, 1 / 2) for _ in range(5)], [torch.randn(1) for _ in range(5)]).size() == torch.Size([])

    def test_tensors_as_dist_args(self):
        self.assertAlmostEqual(
            negative_log_prob(
                [
                    dist.Normal(
                        torch.ones(1, device=DEVICE), 0.5 * torch.ones(1, device=DEVICE),
                    )
                    for _ in range(5)
                ],
                [torch.zeros(1, device=DEVICE) for _ in range(5)],
            ),
            11.1290,
            delta=0.1,
        )


class Test_negative_log_prob_loss(unittest.TestCase):
    def test_aleatoric_heteroskedastic_uncertainty(self):
        loss = negative_log_prob_loss(normal(), torch.zeros(1, device=DEVICE))
        assert isinstance(loss, torch.Tensor)
        self.assertAlmostEqual(loss.item(), 1.4189, delta=0.1)

    def test_size(self):
        loss = negative_log_prob_loss(
            dist.Normal(loc=torch.ones(50), scale=torch.ones(50)), torch.zeros(50),
        )
        assert loss.size() == torch.Size([])


class Test_log_prob_loss(unittest.TestCase):
    def test_aleatoric_heteroskedastic_uncertainty(self):
        loss = log_prob.log_prob_loss(normal(), torch.zeros(1, device=DEVICE))
        assert isinstance(loss, torch.Tensor)
        self.assertAlmostEqual(loss.item(), -1.4189, delta=0.1)

    def test_size(self):
        loss = log_prob.log_prob_loss(
            dist.Normal(loc=torch.ones(50), scale=torch.ones(50)), torch.zeros(50),
        )
        assert loss.size() == torch.Size([])


if __name__ == "__main__":
    unittest.main()
