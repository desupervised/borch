import unittest

import torch

import borch
from borch import distributions
from borch.metrics import rv_metrics
from borch.utils.torch_utils import get_device

DEVICE = get_device()


def test_module_metrics():
    mod = borch.module.Module()
    mod.observe(hello=torch.ones(1))
    mod.hello = distributions.Normal(0, 1)
    mod_metrics = rv_metrics.module_metrics(mod)
    assert isinstance(mod_metrics, dict)
    assert isinstance(mod_metrics["hello"]["mean_squared_error"], torch.Tensor)


def test_module_metrics_with_torch_module_gives_empty_dict():
    assert rv_metrics.module_metrics(torch.nn.Module()) == {}


class Test_metric(unittest.TestCase):
    def test_outputs_dict(self):
        rv = distributions.Normal(
            torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE),
        )
        rv.tensor = torch.randn(2, 4).to(DEVICE)
        met = rv_metrics.all_metrics(rv)
        assert isinstance(met, dict)

    def test_outputs_contains_mse_for_normal(self):
        rv = distributions.Normal(
            torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE),
        )
        rv.tensor = torch.randn(2, 4).to(DEVICE)
        calc_metrics = borch.metrics.rv_metrics.all_metrics(rv)
        assert rv_metrics.mean_squared_error.__name__ in calc_metrics
        assert float(calc_metrics[rv_metrics.mean_squared_error.__name__]) > 0

    def test_outputs_contains_accuracy_for_categorical(self):
        rv = distributions.Categorical(logits=torch.randn(4, device=DEVICE))
        rv.tensor = torch.ones(1).long().to(DEVICE)
        assert rv_metrics.accuracy.__name__ in borch.metrics.rv_metrics.all_metrics(rv)


class Test_mean_squared_error(unittest.TestCase):
    metric = staticmethod(rv_metrics.mean_squared_error)

    def setUp(self):
        self.rv = distributions.Normal(
            torch.zeros(2, 4, device=DEVICE), torch.ones(2, 4, device=DEVICE),
        )
        self.rv.tensor = torch.randn(2, 4).to(DEVICE)

    def test_correct_shape(self):
        mse = self.metric(self.rv)
        assert mse.shape == torch.Size([])


class Test_accuracy(Test_mean_squared_error):
    metric = staticmethod(rv_metrics.accuracy)

    def setUp(self):
        self.rv = distributions.Categorical(logits=torch.randn(4, device=DEVICE))
        self.rv.tensor = torch.ones(1).long().to(DEVICE)
