
import unittest

import numpy as np
import pytest
import torch

from borch.metrics import metrics
from borch.utils.torch_utils import get_device

DEVICE = get_device()


@pytest.mark.parametrize("fn", [metrics.r2_mean_diff_ratio, metrics.r2_score])
def test_r2(fn):
    x = torch.ones(100)
    x[0] = 0
    r_2 = fn(x, x)
    assert isinstance(r_2, torch.Tensor)
    assert float(r_2) == 1


class Test_mean_squared_error(unittest.TestCase):
    def test_correct_answer(self):
        assert metrics.mean_squared_error(torch.ones(2, 3, device=DEVICE), torch.zeros(2, 3, device=DEVICE)).item() == 1.0

    def test_correct_dim(self):
        out = metrics.mean_squared_error(
            torch.ones(2, 3, 1, device=DEVICE), torch.zeros(2, 3, 1, device=DEVICE),
        )
        assert out.shape == torch.Size([])

    @staticmethod
    def test_double():
        np.testing.assert_allclose(
            metrics.mean_squared_error(
                torch.ones(2, 3, 4, 2, device=DEVICE).double(),
                torch.zeros(2, 3, 4, 2, device=DEVICE).double(),
            ).item(),
            1.0,
        )


class Test_accuracy(unittest.TestCase):
    def test_correct_answer(self):
        assert metrics.accuracy(torch.tensor([2, 3], device=DEVICE).long(), torch.tensor([2, 1], device=DEVICE).long()).item() == 0.5

    def test_correct_answer_perfect_acc(self):
        assert metrics.accuracy(torch.ones(2, 3, 2, 4, device=DEVICE).long(), torch.ones(2, 3, 2, 4, device=DEVICE).long()).item() == 1

    def test_correct_dim(self):
        out = metrics.accuracy(
            torch.randint(0, 2, (2, 3, 3, 2), device=DEVICE),
            torch.randint(0, 2, (2, 3, 3, 2), device=DEVICE),
        )
        assert out.shape == torch.Size([])


class Test_accuracy_logit(unittest.TestCase):
    def test_correct_type_out(self):
        assert isinstance(metrics.accuracy_logit(torch.randn(2, 2, device=DEVICE), torch.tensor([0, 1], device=DEVICE).long()), torch.Tensor)

    def test_correctness(self):
        logits = torch.tensor(
            [[0.1, 0.2, 0.4], [0.1, 0.2, 0.4], [0.1, 0.9, 0.4], [0.1, 0.9, 0.4]],
            device=DEVICE,
        )
        targets = torch.tensor([2, 2, 2, 1], device=DEVICE).long()
        out = metrics.accuracy_logit(logits, targets)
        assert out.shape == torch.Size([])
        assert float(out) == 0.75

    def test_correct_in_high_dim(self):
        logits = torch.tensor(
            [[0.1, 0.2, 0.4], [0.1, 0.2, 0.4], [0.1, 0.9, 0.4], [0.1, 0.9, 0.4]],
            device=DEVICE,
        )
        targets = torch.tensor([2, 2, 2, 1], device=DEVICE).long()

        logits, targets = logits.unsqueeze(0), targets.unsqueeze(0)
        out = metrics.accuracy_logit(logits, targets)
        assert out.shape == torch.Size([])
        assert float(out) == 0.75


class Test_confusion_matrix(unittest.TestCase):
    def test_call_with_ints(self):
        y_true = [2, 0, 2, 2, 0, 1]
        y_pred = [0, 0, 2, 2, 0, 2]
        conf_mat, _ = metrics.confusion_matrix(y_pred, y_true)
        truth = np.array([[1, 0, 0], [0, 0, 1], [1 / 3, 0, 2 / 3]])
        assert np.array_equal(conf_mat, truth)

    def test_call_with_strings(self):
        y_true = ["cat", "ant", "cat", "cat", "ant", "bird"]
        y_pred = ["ant", "ant", "cat", "cat", "ant", "cat"]
        conf_mat, _ = metrics.confusion_matrix(y_pred, y_true)
        truth = np.array([[1, 0, 0], [0, 0, 1], [1 / 3, 0, 2 / 3]])
        assert np.array_equal(conf_mat, truth)

    def test_call_with_torch(self):
        y_true = torch.tensor([2, 0, 2, 2, 0, 1])
        y_pred = torch.tensor([0, 0, 2, 2, 0, 2])
        conf_mat, _ = metrics.confusion_matrix(y_pred, y_true)
        truth = np.array([[1, 0, 0], [0, 0, 1], [1 / 3, 0, 2 / 3]])
        assert np.array_equal(conf_mat, truth)


class Test_binary_roc_auc(unittest.TestCase):
    def try_correct_calculation(self, y_pred, y_true):
        assert metrics.binary_roc_auc(y_pred, y_true) == 0.4444444444444444

    def test_correct_calculation_list(self):
        y_pred = [0.9, 0.1, 0.2, 0.8, 0.7, 0.6]
        y_true = [0, 0, 1, 1, 0, 1]
        self.try_correct_calculation(y_pred, y_true)

    def test_correct_calculation_numpy(self):
        y_pred = np.array([0.9, 0.1, 0.2, 0.8, 0.7, 0.6])
        y_true = np.array([0, 0, 1, 1, 0, 1])
        self.try_correct_calculation(y_pred, y_true)

    def test_correct_calculation_tensor(self):
        y_pred = torch.tensor([0.9, 0.1, 0.2, 0.8, 0.7, 0.6])
        y_true = torch.tensor([0, 0, 1, 1, 0, 1])
        self.try_correct_calculation(y_pred, y_true)

    def test_non_equal_length(self):
        with pytest.raises(RuntimeError):
            metrics.binary_roc_auc([1, 0, 1, 1, 2], [0.5, 0.1, 0.3])

    def test_bad_targets(self):
        with pytest.raises(RuntimeError):
            metrics.binary_roc_auc([0.8, 0.9, 0.4], [0, 0, 0])
        with pytest.raises(RuntimeError):
            metrics.binary_roc_auc([0.4, 0.2, 0], [1, 1, 1])
        with pytest.raises(RuntimeError):
            metrics.binary_roc_auc([0.4, 0.2, 0], [0, 1, 0.2])

    def test_bad_predictions(self):
        with pytest.raises(ValueError):
            metrics.binary_roc_auc([1.3, 0.9, 0.4], [0, 1, 0])
        with pytest.raises(ValueError):
            metrics.binary_roc_auc([-0.2, 0.2, 0], [1, 0, 1])
