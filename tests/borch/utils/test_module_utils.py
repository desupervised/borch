from unittest import TestCase

import pytest
import torch
from torch import nn

from borch.nn import Linear
from borch.utils import module_utils


class TestTotalParameters(TestCase):
    def setUp(self):
        self.net = torch.nn.Sequential(
            torch.nn.Linear(4, 5, bias=True),
            torch.nn.Sigmoid(),
            torch.nn.Linear(5, 6, bias=True),
            torch.nn.Sigmoid(),
            torch.nn.Linear(6, 7, bias=True),
            torch.nn.Sigmoid(),
        )

    def test_correctness_of_total_params(self):
        # From the network above we have the following number of trainable
        # parameters (including biases!):
        layer_1 = (4 + 1) * 5
        layer_2 = (5 + 1) * 6
        layer_3 = (6 + 1) * 7
        truth = layer_1 + layer_2 + layer_3
        assert module_utils.total_parameters(self.net) == truth


class TestCopyModuleAttributes(TestCase):
    def setUp(self):
        self.original = torch.nn.Linear(3, 4)
        self.copy = torch.nn.Linear(3, 4)

    def test_paramters_retain_their_class(self):
        module_utils.copy_module_attributes(self.original, self.copy)
        assert isinstance(self.copy.weight, torch.nn.Parameter)

    def test_copy_parameters_are_unique_objects(self):
        module_utils.copy_module_attributes(self.original, self.copy)
        assert self.original.weight is not self.copy.weight


class Test_parameters_not_named(TestCase):
    def test_one_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.parameters_not_named(net, "bad"))) == 2

    def test_nothing_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.parameters_not_named(net, "bad"))) == 3

    def test_remove_scale(self):
        net = Linear(2, 2)
        assert len(list(module_utils.parameters_not_named(net, "scale.param"))) == 2


class Test_parameters_named(TestCase):
    def test_one_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.parameters_named(net, "bad"))) == 1

    def test_nothing_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.parameters_named(net, "bad"))) == 0

    def test_get_scale(self):
        net = Linear(2, 2)
        assert len(list(module_utils.parameters_named(net, "scale.param"))) == 2


class Test_yield_not_named(TestCase):
    def test_one_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.yield_not_named(net.named_parameters(), "bad"))) == 2

    def test_nothing_to_remove(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.yield_not_named(net.named_parameters(), "bad"))) == 3

    def test_remove_scale(self):
        net = Linear(2, 2)
        assert len(list(module_utils.yield_not_named(net.named_parameters(), "scale.param"))) == 2


class Test_yield_named(TestCase):
    def test_one_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("bad", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.yield_named(net.named_parameters(), "bad"))) == 1

    def test_nothing_to_return(self):
        net = torch.nn.Linear(2, 2)
        net.register_parameter("ok", torch.nn.Parameter(torch.randn(4)))
        assert len(list(module_utils.yield_named(net.named_parameters(), "bad"))) == 0

    def test_get_scale(self):
        net = Linear(2, 2)
        assert len(list(module_utils.yield_not_named(net.named_parameters(), "scale.param"))) == 2


def test_get_nested_module():
    expected = nn.Linear(3, 4)
    net = nn.Sequential(
        nn.Sequential(nn.Linear(2, 3), expected),
        nn.Sequential(nn.Linear(4, 5), nn.Linear(5, 6)),
    )
    response = module_utils.get_nested_module(net, ("0", "1"))
    assert response is expected


def test_get_nested_modules():
    expected = (nn.Linear(3, 4), nn.Linear(4, 5))
    net = nn.Sequential(
        nn.Sequential(nn.Linear(2, 3), expected[0]),
        nn.Sequential(expected[1], nn.Linear(5, 6)),
    )
    response = module_utils.get_nested_modules(net, [("0", "1"), ("1", "0")])
    assert response == expected


class Network(nn.Sequential):
    def __init__(self, n_out):
        super().__init__()
        self.one = nn.Linear(3, 4)
        self.two = nn.Linear(4, 5)
        self.thr = nn.Linear(5, n_out)


class TestLoadStateDict(TestCase):
    def setUp(self):
        self.net = Network(n_out=10)
        self.state_dict = self.net.state_dict()

    def test_raises_errors_when_names_mismatched_and_strict_names(self):
        name, tensor = self.state_dict.popitem()
        self.state_dict[name + "2"] = tensor

        with pytest.raises(RuntimeError):
            # This is the basic PyTorch behaviour
            module_utils.load_state_dict(self.net, self.state_dict)

        with pytest.raises(RuntimeError):
            # This is the custom behaviour
            module_utils.load_state_dict(self.net, self.state_dict, strict_shapes=False)

    def test_when_names_mismatched_and_strict_names_is_false(self):
        name, tensor = self.state_dict.popitem()
        self.state_dict[name + "2"] = tensor
        module_utils.load_state_dict(self.net, self.state_dict, strict_names=False)
        module_utils.load_state_dict(
            self.net, self.state_dict, strict_names=False, strict_shapes=False,
        )

    def test_loads_dict_with_mismatched_shapes(self):
        net = Network(n_out=20)
        module_utils.load_state_dict(net, self.state_dict, strict_shapes=False)

    def test_loads_dict_with_mismatched_names_when_strict_shapes_is_false(self):
        _, tensor = self.state_dict.popitem()
        self.state_dict["hello.weight"] = tensor
        module_utils.load_state_dict(
            self.net, self.state_dict, strict_names=False, strict_shapes=False,
        )


def test_copy_if_possible_dict():
    data = {"a": "b"}
    new = module_utils._copy_if_possible(data)
    assert data == new
    assert id(data) != new
