from unittest import TestCase, skipUnless

import numpy.testing as npt
import torch

from borch.utils import torch_utils

DEVICE = torch_utils.get_device()


class Test_grads_to_none(TestCase):
    def test_gradients_to_none(self):
        val = torch.randn(100, requires_grad=True)
        loss = torch.sum(100 * torch.randn(100) * val)
        loss.backward()
        assert val.grad is not None
        torch_utils.grads_to_none([val])
        assert val.grad is None


class TestDetachTensorList(TestCase):
    def test_detach_copy_tensors(self):
        tensor_list = [torch.randn(100, requires_grad=True) for _ in range(10)]
        new_tensor_list = torch_utils.detach_copy_tensors(tensor_list)
        for val in new_tensor_list:
            assert not val.requires_grad


class TestDetachTensorDict(TestCase):
    def test_detach_tensor_dict(self):
        tensor_list = {ii: torch.randn(100, requires_grad=True) for ii in range(10)}
        new_tensor_list = torch_utils.detach_tensor_dict(tensor_list)
        for val in new_tensor_list.values():
            assert not val.requires_grad


class TestUpdateTensorData(TestCase):
    def test_update_tensor_data(self):
        tensor_list = [torch.randn(1, requires_grad=True) for _ in range(10)]
        data_list = [torch.randn(1, requires_grad=True) for _ in range(10)]
        torch_utils.update_tensor_data(tensor_list, data_list)
        for tens, dat in zip(tensor_list, data_list, strict=False):
            assert tens == dat


class Test_dict_values_to_tensor(TestCase):
    def test_just_floats(self):
        temp_dict = {"a": 1, "b": 2}
        tensor_dict = torch_utils.dict_values_to_tensor(temp_dict)
        for val in tensor_dict.values():
            assert isinstance(val, torch.Tensor)

    def test_does_not_change_excisting_tensors(self):
        temp_dict = {"a": 1, "b": torch.ones(1)}
        tensor_dict = torch_utils.dict_values_to_tensor(temp_dict)
        assert id(tensor_dict["b"]) == id(tensor_dict["b"])


class Test_is_optimizable_leaf_tensor(TestCase):
    def test_requires_grad_true(self):
        var = torch.randn(1, requires_grad=True)
        assert torch_utils.is_optimizable_leaf_tensor(var)

    def test_requires_grad_false(self):
        var = torch.randn(1, requires_grad=False)
        assert not torch_utils.is_optimizable_leaf_tensor(var)


# class Test_is_all_cuda(TestCase):
#     def test_all_same_device(self):
#         variables = [torch.randn(1, device=DEVICE) for _ in range(10)]
#         if torch.cuda.is_available():
#             self.assertTrue(torch_utils.is_all_cuda(variables))
#         else:
#             self.assertFalse(torch_utils.is_all_cuda(variables))
#
#     def test_all_cpu(self):
#         varaibels = [torch.randn(1) for _ in range(10)]
#         self.assertFalse(torch_utils.is_all_cuda(varaibels))
#
#     def test_mix_cpu_gpu(self):
#         if torch.cuda.is_available():
#             varaibels = [torch.randn(1) for _ in range(10)]
#             varaibels.append(torch.randn(1, device=DEVICE))
#             self.assertFalse(torch_utils.is_all_cuda(varaibels))
#
#     def test_empty_list(self):
#         self.assertFalse(torch_utils.is_all_cuda([]))


class TestOneHot(TestCase):
    def test_correctness_of_output(self):
        labels = torch.LongTensor([0, 1, 1, 3])
        response = torch_utils.one_hot(labels, n_classes=5)
        truth = torch.tensor(
            [[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 0, 1, 0.0]],
        )
        assert torch.equal(truth, response)

    @skipUnless(torch.cuda.is_available(), "Requires cuda")
    def test_works_for_gpu(self):
        labels = torch.cuda.LongTensor([0, 1, 1, 3])
        response = torch_utils.one_hot(labels, n_classes=5)
        truth = torch.tensor(
            [[1, 0, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 0, 0, 1, 0.0]],
        ).to(labels.device)
        assert torch.equal(truth, response)


class Test_gradient(TestCase):
    @staticmethod
    def test_one_param():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        grad = torch_utils._gradient(x.pow(2).sum(), x, create_graph=True)
        npt.assert_array_equal(grad.detach(), torch.tensor([4.0, 10.0]))


class Test_jacobian(TestCase):
    @staticmethod
    def test_one_param():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        jac = torch_utils.jacobian(x.pow(2).sum(), x, create_graph=True)
        npt.assert_array_equal(jac.detach(), torch.tensor([[4.0, 10.0]]))

    @staticmethod
    def test_two_params():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        y = torch.tensor([1.0, 2.0], requires_grad=True)
        jac = torch_utils.jacobian(x.pow(y).prod(), [x, y], create_graph=True)
        npt.assert_allclose(
            jac.detach(),
            torch.tensor([[25.0000, 20.0000, 34.6574, 80.4719]]),
            rtol=1e-4,
        )

    @staticmethod
    def test_two_params_two_outputs():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        y = torch.tensor([1.0, 2.0], requires_grad=True)
        jac = torch_utils.jacobian(tuple(x.pow(y)), [x, y], create_graph=True)
        npt.assert_allclose(
            jac.detach(),
            torch.tensor([[1, 0, 1.386394, 0], [0.0, 10.0, 0.0, 40.235947]]),
            rtol=1e-4,
        )


class Test_hessian(TestCase):
    @staticmethod
    def test_one_param():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        hess = torch_utils.hessian(x.pow(2).sum(), x, create_graph=True)
        npt.assert_array_equal(hess.detach(), torch.tensor([[2.0, 0.0], [0.0, 2.0]]))

    @staticmethod
    def test_two_params():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        y = torch.tensor([1.0, 2.0], requires_grad=True)
        hess = torch_utils.hessian(x.pow(y).prod(), [x, y], create_graph=True)
        npt.assert_allclose(
            hess.detach(),
            torch.tensor(
                [
                    [0.0, 10.0, 42.328682, 40.235947],
                    [10.0, 4.0, 13.862944, 42.18876],
                    [42.328682, 13.862944, 24.022652, 55.77887],
                    [40.235947, 42.18876, 55.77887, 129.5145],
                ],
            ),
            rtol=1e-4,
        )

    @staticmethod
    def test_two_params_not_mixed_in_hessian():
        x = torch.tensor([2.0, 5.0], requires_grad=True)
        y = torch.tensor([1.0, 2.0], requires_grad=True)
        hess = torch_utils.hessian(
            (x.pow(2).prod() + y).sum(), [x, y], create_graph=True,
        )
        npt.assert_allclose(
            hess.detach(),
            torch.tensor(
                [
                    [100.0, 80.0, 0.0, 0.0],
                    [80.0, 16.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0],
                ],
            ),
            rtol=1e-4,
        )
