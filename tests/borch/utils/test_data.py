import numpy.testing as npt
import torch
from pytest import fixture
from torch.utils.data import TensorDataset

from borch.utils.data import AddDatasetLength


@fixture
def dataset():
    inps = torch.arange(10 * 5, dtype=torch.float32).view(10, 5)
    tgts = torch.arange(10 * 5, dtype=torch.float32).view(10, 5)
    return TensorDataset(inps, tgts)


def test_add_dataset_length(dataset):
    len_ds = AddDatasetLength(dataset)
    assert len(dataset) == len_ds[0][0]
    for i in range(2):
        npt.assert_allclose(dataset[0][i], len_ds[0][1][i])


def test_correct_length(dataset):
    len_ds = AddDatasetLength(dataset)
    assert len(dataset) == len_ds[0][0] == len(len_ds)


def test_getattr_propegates(dataset):
    len_ds = AddDatasetLength(dataset)
    for x, y in zip(len_ds.tensors, dataset.tensors, strict=False):
        npt.assert_allclose(x, y)
