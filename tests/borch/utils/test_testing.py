from io import StringIO
from types import GeneratorType
from unittest import TestCase, mock

import pytest

from borch.utils import testing


def _test():
    pass


@mock.patch(f"{testing.__name__}.glob")
def test_get_source_files(mocked):
    package = "foo"

    # Check that the test_re performs correctly
    mocked.return_value = (
        f"{package}/test/test_me.py",  # Should match test_re
        f"{package}/source/source.py",  # Shouldn't match test_re
    )
    response = tuple(testing.get_source_files((package,)))

    # Only the first element should have matched test_re
    assert response == (mocked.return_value[1:])


def test_filepath_to_import_path():
    for filepath, truth in (
        ("ppl/hello/poll.py", "ppl.hello.poll"),
        ("poll.py", "poll"),
    ):
        assert testing.filepath_to_import_path(filepath) == truth


@mock.patch(f"{testing.__name__}.import_module")
@mock.patch(f"{testing.__name__}.get_source_files", return_value=("a.py"))
@mock.patch(f"{testing.__name__}.testmod")
class TestRunDoctests(TestCase):
    def test_yields_callable(self, *_):
        response = testing.run_doctests("")
        assert isinstance(response, GeneratorType)
        assert callable(next(response))

    def test_passes_for_passing_doctest(self, mock_testmod, *_):
        mock_testmod.return_value = mock.Mock(failed=False)
        for test in testing.run_doctests(""):
            test()

    def test_fails_for_failing_doctest(self, mock_testmod, *_):
        mock_testmod.return_value = mock.Mock(failed=True)
        with pytest.raises(AssertionError):
            for test in testing.run_doctests(""):
                test()


@mock.patch(f"{testing.__name__}.open")
def test_execfile(mocked):
    return_value = 5
    key = "b"
    func_name = "func"

    # Construct a dummy source code file which simply defines and executes
    # a function. The indentifiers `func_name`, `return_value` and `key`
    # are all reused in the tests.
    code = f"""
def {func_name}():
    return {return_value}

{key} = {func_name}()"""
    mocked.return_value.__enter__ = lambda _: StringIO(code)
    response = testing._execfile("")

    # `response` contains all objects created during the execution of `code`;
    # test that it contains the appropriate values.
    assert response[key] == return_value
    assert func_name in response


@mock.patch(f"{testing.__name__}.ScriptTest.path", new_callable=mock.PropertyMock)
@mock.patch(f"{testing.__name__}._execfile")
class TestScriptTest(TestCase):

    @mock.patch(f"{testing.__name__}.ScriptTest.before_script")
    def test_setup_calls_before_script(self, mock_before_script, *_args):
        script_test = testing.ScriptTest()
        script_test.setUpClass()
        mock_before_script.assert_called()

    def test_setup_calls_execfile(self, mock_execfile, _):
        script_test = testing.ScriptTest()
        script_test.setUpClass()
        mock_execfile.assert_called()


def write_small_file(file_path):
    with open(file_path, "w+") as f:
        f.write("Some text")
