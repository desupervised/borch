from unittest import TestCase

import numpy as np

from borch.utils import numpy_utils


class Test_nearest_pos_def_mat(TestCase):
    def test_for_random_mats(self):
        for j in range(5, 10):
            j *= 10
            mat = np.random.randn(j, j)
            mat = numpy_utils.nearest_pos_def_mat(mat)
            assert numpy_utils.is_pos_definite(mat)

    @staticmethod
    def test_with_already_pos_def_mat():
        mat = np.array([[2, -1, 0], [-1, 2, -1], [0, -1, 2]])
        out = numpy_utils.nearest_pos_def_mat(mat)
        np.testing.assert_almost_equal(mat, out, decimal=1e-5)


class Test_is_pos_definitve(TestCase):
    def test_non_pd_gives_false(self):
        x = np.array([[1, 2], [2, 1]])
        assert not numpy_utils.is_pos_definite(x)

    def test_pd_gives_true(self):
        x = np.array([[2, -1, 0], [-1, 2, -1], [0, -1, 2]])
        assert numpy_utils.is_pos_definite(x)
