from unittest import TestCase, skipUnless

import torch

from borch import distributions, nn
from borch.utils.state_dict import (
    copy_state_dict,
    saveable_state_dict,
    state_dict_to_device_,
)
from borch.utils.torch_utils import get_device


class Test_saveable_state_dict(TestCase):
    def setUp(self):
        self.net = Net()
        x = torch.randn(3)
        self.net(x)

    def tearDown(self):
        del self.net

    def test_state_dict_is_dict(self):
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        assert isinstance(new_state_dict, dict)

    @skipUnless(torch.cuda.is_available(), "requires GPU")
    def test_state_dict_is_cpu_when_observing_on_gpu(self):
        self.net.to("cuda")
        self.net.observe(classification=torch.tensor(0, device="cuda"))
        state_dict = self.net.state_dict()
        new_state_dict = saveable_state_dict(state_dict)
        for tensor in new_state_dict.values():
            assert tensor.device.type == "cpu"


class Test_state_dict_to_device(TestCase):
    @staticmethod
    def test_sending_state_dict_to_float16():
        net = Net()
        state_dict = state_dict_to_device_(net.state_dict(), torch.float64)
        for tensor in state_dict.values():
            assert tensor.dtype == torch.float64

    def test_all_params_are_on_cpu(self):
        net = Net()
        net(torch.randn(3))
        net.to(get_device())

        state_dict = state_dict_to_device_(net.state_dict(), "cpu")
        for val in state_dict.values():
            if hasattr(val, "parameters"):
                for par in val.parameters():
                    assert par.device.type == "cpu"
            assert val.device.type == "cpu"


class Test_copy_state_dict(TestCase):
    def setUp(self):
        net = Net()
        net(torch.randn(3))
        self.state_dict = net.state_dict()

    def tearDown(self):
        del self.state_dict

    def test_original_state_dict_stays_same(self):
        before_ids = get_state_dict_ids(self.state_dict)
        copy_state_dict(self.state_dict)
        after_ids = get_state_dict_ids(self.state_dict)
        for before_id, after_id in zip(before_ids, after_ids, strict=False):
            assert before_id == after_id

    def test_new_state_dict_is_new(self):
        original_ids = get_state_dict_ids(self.state_dict)
        copied_ids = get_state_dict_ids(copy_state_dict(self.state_dict))
        for original_id, copied_id in zip(original_ids, copied_ids, strict=False):
            assert original_id != copied_id

    def test_original_state_dict_is_untouched_after_assigning_data(self):
        state_dict = {"rv": distributions.Normal(0, 1)}
        new_state_dict = copy_state_dict(state_dict)
        new_state_dict["rv"].tensor = torch.Tensor(1000)
        assert state_dict["rv"].item() != 1000


def get_state_dict_ids(state_dict):
    ids = []
    for val in state_dict.values():
        if hasattr(val, "parameters"):
            for par in val.parameters():
                ids.append(id(par))
            ids.append(id(val))
    return ids


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer_1 = nn.Linear(3, 3)

    # pylint: disable=arguments-differ, attribute-defined-outside-init
    def forward(self, x):
        x = self.layer_1(x)
        self.classification = distributions.Categorical(logits=x)
        return self.classification
