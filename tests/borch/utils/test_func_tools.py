import unittest

from borch.utils import func_tools


class TestDisableDoctests(unittest.TestCase):
    def test_correctness_of_output(self):
        string = """
        Here is some text. Examples include...

        Example:
            >>> # doing something
            >>> print("hello")
            'hello'
        """
        new_string = func_tools.disable_doctests(string)

        expected = """
        Here is some text. Examples include...

        Example:
            >>>
            >> # doing something
            >> print("hello")
            'hello'
        """
        assert new_string == expected


class TestAssignDocs(unittest.TestCase):
    def setUp(self):
        # create a placeholder class
        self.new_cls = type("Dummy", (object,), {"__init__": lambda x: x})
        self.old_cls = type("Dummy", (object,), {"__init__": lambda x: x})

        self.cls_docs = "Here is my class docstring."
        self.init_docs = "Here is my init docstring."
        self.old_cls.__doc__ = self.cls_docs
        self.old_cls.__init__.__doc__ = self.init_docs

    def test_docstrings_correctly_updated_on_class_level(self):
        func_tools.assign_docs(self.new_cls, self.old_cls, "PREFIX")
        assert f"PREFIX\n\n{self.cls_docs}" == self.new_cls.__doc__

    def test_docstrings_correctly_updated_on_init_function(self):
        func_tools.assign_docs(self.new_cls, self.old_cls, "PREFIX")
        assert f"PREFIX\n\n{self.init_docs}" == self.new_cls.__init__.__doc__


class Test_args_to_kwargs(unittest.TestCase):
    def _verify_kwargs(self, kwargs, expected_kwargs):
        for key, val in expected_kwargs.items():
            assert val == kwargs[key]

    def _test_fn(self, var1, var2):
        pass

    def test_all_args(self):
        expected_kwargs = {"var1": 1, "var2": 2}
        self._verify_kwargs(
            func_tools.args_to_kwargs(self._test_fn, 1, 2), expected_kwargs,
        )

    def test_all_kargs(self):
        expected_kwargs = {"var1": 4, "var2": 5}
        self._verify_kwargs(
            func_tools.args_to_kwargs(self._test_fn, **expected_kwargs), expected_kwargs,
        )

    def test_mix_kwar_args(self):
        expected_kwargs = {"var1": 2, "var2": 10}
        self._verify_kwargs(
            func_tools.args_to_kwargs(self._test_fn, 2, var2=10), expected_kwargs,
        )

    def test_fn_without_self(self):
        def test_fn(var1, var2):  # pylint: disable=unused-argument
            pass

        expected_kwargs = {"var1": 2, "var2": 10}
        self._verify_kwargs(
            func_tools.args_to_kwargs(test_fn, 2, var2=10), expected_kwargs,
        )


class Test_replace_none_with_argument(unittest.TestCase):
    def test_replace_none_with_argument(self):
        standard_value = 10

        def test(val):
            return val

        new_test = func_tools.replace_none_with_argument(standard_value, test)
        assert new_test() == standard_value
