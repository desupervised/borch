import sys
from unittest import TestCase, mock

from borch.utils import namespace_tools

MODULE_PATH = "borch.utils.namespace_tools"


class TestGetSubclassMembers(TestCase):
    # There's no obvious and nice way to test this without involving an
    # external dependency..

    @staticmethod
    @mock.patch(f"{MODULE_PATH}.inspect.getmembers")
    def test_calls_inspect_getmemebers(mocked):
        namespace_tools.get_subclass_members(sys, object)
        mocked.assert_called()

    def test_loader_returned_from_object_subclasses_in_sys(self):
        ans = namespace_tools.get_subclass_members(sys, object)
        assert "__loader__" in [e[0] for e in ans]


class Test_get_instances(TestCase):
    # There's no obvious and nice way to test this without involving an
    # external dependency..

    @staticmethod
    @mock.patch(f"{MODULE_PATH}.inspect.getmembers")
    def test_calls_inspect_getmemebers(mocked):
        namespace_tools.get_instances(sys, object)
        mocked.assert_called()

    def test_loader_returned_from_object_subclasses_in_sys(self):
        ans = namespace_tools.get_instances(sys, object)
        assert "__loader__" in [e[0] for e in ans]


class TestCreateAugmentedClasses(TestCase):
    def setUp(self):
        # create dummy module name for caller
        self.caller_name = "caller"

        # create a mock sys.modules dict, we expect an attribute to be set on
        # the value of `caller_name`
        self.sys_modules = {self.caller_name: mock.MagicMock()}

        # create a dummy class to be augmented
        self.cls = type("classname", (object,), {})

        # create a class factory as a mock object which just returns its input
        self.class_factory = mock.MagicMock(side_effect=lambda x: x)

        with mock.patch.object(namespace_tools.sys, "modules", self.sys_modules):
            with mock.patch.object(
                namespace_tools,
                "get_subclass_members",
                lambda x, y: [("name", self.cls)],
            ):
                namespace_tools.extend_module(
                    self.caller_name,
                    namespace_tools.create_augmented_classes(
                        module=mock,  # use anything
                        parent=mock.MagicProxy,  # use anything
                        class_factory=self.class_factory,
                    ),
                )

    def test_class_added_to_caller_module(self):
        added = getattr(self.sys_modules[self.caller_name], self.cls.__name__)
        assert added is self.cls

    def test_class_factory_called_on_target_class(self):
        self.class_factory.assert_called_once()
