import pickle as pkl
import unittest
from io import BytesIO
from itertools import chain

import pytest
import torch
from torch.nn import Parameter

import borch
from borch import Transform


@pytest.fixture
def par():
    value = Parameter(torch.ones(3))
    return Transform(torch.exp, value)


class TestTransform(unittest.TestCase):
    def setUp(self):
        self.shape = (3,)
        self.value = Parameter(torch.ones(self.shape))
        self.transform = torch.exp
        self.param = Transform(self.transform, self.value)

    def test_repr(self):
        prefix = "Transform:"
        assert repr(self.param).startswith(prefix)

    def test_parameters_returns_tensor_when_grad_required(self):
        assert len(tuple(self.param.parameters())) == 1

    def test_transformation_applied_at_init(self):
        assert torch.equal(self.param.tensor, self.transform(self.param.param))

    def test_self_and_all_parameters_are_cast_to_double_when_applied(self):
        self.param.double()
        for param in chain(self.param.tensor, self.param.parameters()):
            assert param.dtype is torch.float64

    def test_same_value_after_pickle(self):
        f = BytesIO()
        pkl.dump(self.param, f)
        f.seek(0)
        pickled_param = pkl.load(f)
        torch.equal(pickled_param.param, self.param.param)
        torch.equal(pickled_param.tensor, self.param.tensor)

    def test_several_backward(self):
        mu = Parameter(torch.ones(2))
        t_par = Transform(torch.exp, mu)
        for _ in range(3):
            loss = 0
            for _ in range(3):
                t_par()
                loss += 2 * torch.sum(t_par)
            loss.backward()
            assert mu.grad is not None


def test_recalculate_paramater():
    def randn(val):
        return torch.randn_like(val)

    param = Transform(randn, torch.ones(2, 3))
    val = param.tensor
    param()
    assert not torch.equal(param.tensor, val)


def test_rv_argument():
    param = Transform(torch.exp, borch.distributions.Normal(0, 1))
    assert isinstance(param.tensor, torch.Tensor)
    assert len(list(borch.random_variables(param, True, True))) == 2
    assert len(list(borch.random_variables(param, False, True))) == 1
    assert len(list(borch.random_variables(param, True, False))) == 1
    assert len(list(param.parameters())) == 2


def test_isinstance_borch_module(par):
    assert isinstance(par, borch.Module)


def test_isinstance_graph(par):
    assert isinstance(par, borch.Graph)


def test_can_set_posterior():
    param = Transform(
        torch.exp,
        borch.distributions.Normal(0, 1),
        posterior=borch.posterior.ScaledNormal(),
    )
    assert isinstance(param.posterior, borch.posterior.ScaledNormal)
