import unittest

import torch

from borch.optimizer_collection import OptimizersCollection, update_opt_params


class Test_OptimizersCollection(unittest.TestCase):
    def test_adam(self):
        var1 = torch.tensor([10.0], requires_grad=True)
        var2 = torch.tensor([10.0], requires_grad=True)
        optimizer = OptimizersCollection(optimizer=torch.optim.Adam, lr=1)

        def objective(var1, var2):
            return var1 * var2

        for _ in range(100):
            optimizer.zero_grad()
            loss = objective(var1, var2)
            loss.backward()
            optimizer.step([var1, var2])

        self.assertAlmostEqual(var1, 0, delta=0.1)
        self.assertAlmostEqual(var2, 0, delta=0.1)

    def test_optimizer_with_closure(self):
        for optim in [torch.optim.LBFGS, torch.optim.Adam]:
            var1 = torch.tensor(
                [0.5], requires_grad=True,
            )
            optimizer = OptimizersCollection(optimizer=optim, lr=0.01)
            for _ in range(100):
                optimizer.zero_grad()

                def closure():
                    loss = 0.25 * var1**2
                    loss.backward()
                    return loss

                optimizer.step([var1], closure=closure)
            self.assertAlmostEqual(var1, 0, delta=0.1)


class Test_update_opt_params(unittest.TestCase):
    def test_add_parms(self):
        var1 = torch.tensor([10.0], requires_grad=True)
        var2 = torch.tensor([10.0], requires_grad=True)
        optimizer = torch.optim.Adam([var1])
        assert optimizer.param_groups[-1]["params"][0] is not var2
        update_opt_params(optimizer, [var2])
        assert optimizer.param_groups[-1]["params"][0] is var2


if __name__ == "__main__":
    unittest.main()
