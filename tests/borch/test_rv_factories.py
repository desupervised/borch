from unittest import TestCase, mock

import numpy as np
import torch
from torch import Tensor

from borch import RandomVariable, rv_factories
from borch import distributions as dist


class TestParameterToNormalRV(TestCase):
    def setUp(self):
        self.shape = (1, 2, 3)
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = rv_factories.parameter_to_normal_rv("param", self.param)

    def assertEqual(self, first, second, msg=None):  # noqa: N802
        if isinstance(first, Tensor) and isinstance(second, Tensor):
            assert torch.equal(first, second)
        else:
            assert first == second, msg

    def test_values_of_arguments_are_correctly_set(self):
        values = {"prior_mean": 3, "prior_sd": 100}
        rv = rv_factories.parameter_to_normal_rv("param", self.param, **values)

        def _create_tensor(name):
            return torch.ones(self.shape) * values[name]

        assert (rv.loc == _create_tensor("prior_mean")).all()
        assert (rv.scale == _create_tensor("prior_sd")).all()

    def test_returned_object_is_random_variable(self):
        assert isinstance(self.rv, RandomVariable)

    def test_rv_data_is_the_same_as_parameter_data(self):
        assert (self.rv.data == self.param.data).all()

    def test_priors_dont_require_gradients_when_set_with_numbers(self):
        rv = rv_factories.parameter_to_normal_rv(
            "param", self.param, prior_mean=1, prior_sd=1,
        )
        assert not rv.distribution.loc.requires_grad
        assert not rv.distribution.scale.requires_grad

    def test_priors_require_gradients_when_set_with_such_tensors(self):
        p_mean = torch.ones(*self.shape, requires_grad=True)
        p_sd = torch.ones(*self.shape, requires_grad=True)
        rv = rv_factories.parameter_to_normal_rv(
            "param", self.param, prior_mean=p_mean, prior_sd=p_sd,
        )
        assert rv.distribution.loc.requires_grad
        assert rv.distribution.scale.requires_grad


class Test_parameter_to_scaled_normal_rv(TestCase):
    rv_factory = staticmethod(rv_factories.parameter_to_scaled_normal_rv)
    scale = 1

    def setUp(self):
        self.shape = (1, 2, 3)
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = self.rv_factory("param", self.param)

    def test_correct_scale(self):
        self.assertAlmostEqual(self.rv.scale.mean().item(), self.scale)

    def test_correct_loc(self):
        self.assertAlmostEqual(self.rv.loc.mean().item(), 0)

    def test_correct_distribution(self):
        assert isinstance(self.rv, dist.Normal)

    def test_returns_random_variable(self):
        assert isinstance(self.rv, RandomVariable)

    def test_dist_args_is_leaf(self):
        assert self.rv.distribution.loc.is_leaf
        assert self.rv.distribution.scale.is_leaf

    def test_dist_args_requires_no_grad(self):
        assert not self.rv.distribution.loc.requires_grad
        assert not self.rv.distribution.scale.requires_grad


class Test_kaiming_normal_rv(Test_parameter_to_scaled_normal_rv):
    rv_factory = staticmethod(rv_factories.kaiming_normal_rv)
    scale = 0.40824833512306213


class Test_xavier_normal_rv(Test_parameter_to_scaled_normal_rv):
    rv_factory = staticmethod(rv_factories.xavier_normal_rv)
    scale = 0.4714045226573944


class TestApplyRVFactory(TestCase):
    def setUp(self):
        self.module = torch.nn.Module()
        self.param = torch.nn.Parameter(torch.ones(3))
        self.module.param = self.param


class Test_priors_to_rv(TestCase):
    rv_factory = staticmethod(rv_factories.priors_to_rv)

    def setUp(self):
        self.shape = (3, 2, 3)
        self.loc = torch.randn(1, requires_grad=True)
        self.pdist = dist.Normal(self.loc, 10)
        self.priors = {"param": self.pdist}
        self.param = torch.nn.Parameter(torch.rand(*self.shape))
        self.rv = self.rv_factory(
            "param", self.param, self.priors, rv_factories.parameter_to_normal_rv,
        )

    @mock.patch.object(rv_factories, "parameter_to_normal_rv")
    def test_uses_propper_fallback(self, mocked):
        self.rv_factory("param", self.param, {}, rv_factories.parameter_to_normal_rv)
        mocked.assert_called()

    def test_returns_param_if_no_fallback(self):
        returned_param = self.rv_factory("param", self.param, {}, None)
        assert not isinstance(returned_param, RandomVariable)
        assert torch.eq(returned_param, self.param).all()

    def test_correct_scale(self):
        self.assertAlmostEqual(self.rv.scale.mean().item(), self.pdist.scale)

    def test_correct_loc(self):
        self.assertAlmostEqual(
            self.rv.loc.mean().item(), self.pdist.loc.mean().item(), delta=1e-6,
        )

    def test_correct_distribution(self):
        assert isinstance(self.rv, dist.Normal)

    def test_returns_random_variable(self):
        assert isinstance(self.rv, RandomVariable)

    def test_dist_args_is_leaf(self):
        assert not self.rv.distribution.loc.is_leaf
        assert self.rv.distribution.scale.is_leaf

    def test_dist_args_requires_no_grad(self):
        assert self.rv.distribution.loc.requires_grad
        assert not self.rv.distribution.scale.requires_grad

    def test_backward_once(self):
        self.rv.exp().sum().backward()
        assert self.loc.grad is not None

    def test_backward_several_times(self):
        self.rv.rsample()
        self.rv.exp().sum().backward()
        self.rv.rsample()
        self.rv.exp().sum().backward()
        assert self.loc.grad is not None


class Test_infinitely_wide_rv(TestCase):
    def setUp(self):
        self.parameter = torch.randn(3, 3, 3)
        self.infinite_rv = rv_factories.parameter_to_maxwidth_uniform(
            "name", self.parameter,
        )

    def test_all_values_equally_probable(self):
        ones = torch.ones(3, 3, 3)
        assert ones.mean() != self.parameter.mean()
        mean1 = self.infinite_rv.distribution.log_prob(self.parameter).mean()
        mean2 = self.infinite_rv.distribution.log_prob(ones).mean()
        assert mean1 == mean2

    def test_var_is_inf(self):
        assert not np.isfinite(self.infinite_rv.sample().var().item())


class Test_delta_rv(TestCase):
    def setUp(self):
        self.parameter = torch.randn(3, 3, 3)
        self.delta_rv = rv_factories.delta_rv("name", self.parameter)

    def test_is_rv(self):
        assert isinstance(self.delta_rv, RandomVariable)

    def test_returns_right_value(self):
        assert self.delta_rv.sample().sum() == self.parameter.sum()
