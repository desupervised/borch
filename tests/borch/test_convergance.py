import numpy.testing as npt
import pytest
import torch
from torch import optim
from torch.nn import Parameter
from torch.nn.utils import clip_grad_norm_

import borch
from borch import distributions as dist
from borch import infer
from borch.random_variable import RVPair
from borch.utils.torch_utils import seed

pytestmark = pytest.mark.integration


def train(model, forward=None, subsamples=3, epochs=200, opt=None, clip_grad=10):
    if opt is None:
        opt = optim.AdamW(model.parameters(), lr=0.1)
    if forward is None:
        forward = model
    for _ in range(epochs):
        opt.zero_grad()
        loss = 0
        for _ in range(subsamples):
            borch.sample(model)
            forward()
            loss += infer.vi_loss(**borch.pq_to_infer(model))
        loss /= subsamples
        loss.backward()
        clip_grad_norm_(model.parameters(), clip_grad)
        opt.step()


def test_scale_normal_converges_to_prior():
    class Model(borch.Module):
        def __init__(self):
            super().__init__(posterior=borch.posterior.ScaledNormal(0.1))
            self.rv = dist.Normal(0, 1)

        def forward(self):
            return self.rv

    model = Model()
    npt.assert_allclose(float(model.posterior.rv.scale), 0.1)
    opt = optim.AdamW(model.parameters(), lr=0.05)
    train(model, opt=opt, epochs=500)
    npt.assert_allclose(
        float(model.posterior.rv.scale), float(model.prior.rv.scale), rtol=0.25,
    )


def linear_regression_data(
    n_data_points=1000, weight=1, bias=1, noise=0.01, device=None,
):
    x = torch.linspace(0, 100, steps=n_data_points)
    y = weight * x + bias + noise * torch.randn(n_data_points)
    if device is not None:
        x, y = x.to(device), y.to(device)
    return x, y


def test_linear_regression():
    seed(1)

    class Model(borch.Module):
        def __init__(self):
            super().__init__()
            self.weight = RVPair(
                dist.Normal(0, 5),
                dist.Normal(
                    Parameter(torch.tensor(2.0)),
                    borch.Transform(
                        torch.nn.functional.softplus, Parameter(torch.tensor(0.1)),
                    ),
                ),
            )
            self.bias = dist.Normal(0, 5)

        def forward(self, x):
            loc = self.weight * x + self.bias
            self.y = dist.Normal(loc, Parameter(torch.tensor(0.1), requires_grad=False))

    model = Model()
    weight, bias = 3, 2
    x, y = linear_regression_data(weight=weight, bias=bias)
    model.observe(y=y)
    train(model, forward=lambda: model(x), epochs=200)
    npt.assert_allclose(
        float(model.weight.posterior.distribution.loc), weight, rtol=0.2,
    )
    npt.assert_allclose(float(model.posterior.bias.loc), bias, rtol=0.2)
    npt.assert_allclose(float(model.posterior.y.scale), 0.1)


def test_nested_in_torch_module_linear_regression():
    class Model(borch.Module):
        def __init__(self):
            super().__init__(posterior=borch.posterior.ScaledNormal(0.1))
            self.weight = dist.Normal(0, 5)
            self.bias = dist.Normal(0, 5)

        def forward(self, x):
            loc = self.weight * x + self.bias
            self.y = dist.Normal(loc, Parameter(torch.tensor(0.1), requires_grad=False))
            return loc

    class Outer(torch.nn.Module):
        def __init__(self):
            super().__init__()
            self.model = Model()

        def forward(self, x):
            return self.model(x)

    model = Outer()
    weight, bias = 3, 2
    x, y = linear_regression_data(weight=weight, bias=bias)
    model.model.observe(y=y)
    train(model, forward=lambda: model(x), epochs=400)
    npt.assert_allclose(float(model.model.posterior.weight.loc), weight, rtol=0.2)
    npt.assert_allclose(float(model.model.posterior.bias.loc), bias, rtol=0.2)


def test_linear_linear_regression():
    class Model(borch.nn.Module):
        def __init__(self):
            super().__init__()
            self.model = torch.nn.Sequential(borch.nn.Linear(1, 1))

        def forward(self, x):
            loc = self.model(x.view(-1, 1)).view(-1)
            self.y = dist.Normal(loc, Parameter(torch.tensor(0.1), requires_grad=False))
            return loc

    model = Model()
    weight, bias = 0.5, 0.5
    x, y = linear_regression_data(weight=weight, bias=bias)
    model.observe(y=y)
    train(model, forward=lambda: model(x), epochs=400)
    npt.assert_allclose(
        float(model.model._modules["0"].posterior.weight), weight, rtol=0.1,
    )
    npt.assert_allclose(float(model.model._modules["0"].posterior.bias), bias, rtol=0.1)


def test_mlp_linear_regression():
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    class Model(borch.nn.Module):
        def __init__(self):
            super().__init__()
            self.model = torch.nn.Sequential(
                borch.nn.Linear(1, 3), torch.nn.ReLU(), borch.nn.Linear(3, 1),
            )

            self.scale = Parameter(torch.tensor(0.1), requires_grad=False)

        def forward(self, x):
            loc = self.model(x.view(-1, 1)).view(-1)
            self.y = dist.Normal(loc, self.scale)
            return loc

    model = Model()
    model.to(device)
    x, y = linear_regression_data(weight=0.5, bias=0.5, device=device)
    model.observe(y=y)
    train(model, forward=lambda: model(x), epochs=500)
    pred = model(x).cpu()
    y = y.cpu()
    npt.assert_allclose(((y - pred.detach().numpy()).abs() / y).mean(), 0, atol=0.1)


def test_linear_regression_with_transform():
    seed(1)
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    class Model(borch.Module):
        def __init__(self):
            super().__init__()
            self.weight = dist.Normal(0, 5)
            self.bias = borch.Transform(
                lambda x: x.clamp(min=-2, max=5), dist.Normal(0, 5),
            )
            self.scale = Parameter(torch.tensor(0.1), requires_grad=False)

        def forward(self, x):
            loc = self.weight * x + self.bias
            self.y = dist.Normal(loc, self.scale)

    model = Model()
    model.to(device)
    weight, bias = 3, 2
    x, y = linear_regression_data(weight=weight, bias=bias, device=device)
    model.observe(y=y)
    train(model, forward=lambda: model(x), epochs=200)
    npt.assert_allclose(float(model.posterior.weight.loc), weight, rtol=0.2)
    npt.assert_allclose(float(model.bias.posterior.param.loc), bias, rtol=0.2)


def test_infer_observed_values():
    loc, scale = 2, 2
    data = torch.empty(2000).normal_(loc, scale)
    model = borch.Module()
    model.observe(y=data)

    def forward(model):
        model.mean = dist.Normal(0, 5)
        model.std = dist.Normal(3, 5)
        std = model.std.clamp(min=0.1)
        model.y = dist.Normal(model.mean, std)

    forward(model)
    train(model, forward=lambda: forward(model), epochs=200)
    npt.assert_allclose(float(model.posterior.mean.loc), loc, rtol=0.2)
    npt.assert_allclose(float(model.posterior.std.loc), scale, rtol=0.2)


def test_infer_observed_values_with_a_transform():
    loc, scale = 2, 2
    data = torch.empty(2000).normal_(loc, scale)
    model = borch.Module()
    model.observe(y=data)
    # if the transform is created in the forward, a new posterior and parameter
    # gets created each time. This is somthing we could modify if we would like to.
    #
    # On a related note, today we do NOT support the syntax(although there
    # is partial suppport for this already)
    # Normal(Normal(0, 1), 1)
    # as all the RV`s are torch.modules and not borch.modules + and some posteriors
    # like `posterior.Automatic`
    # does not account for it correctly today, this is also something we could
    # think about modifying.
    model.std = borch.Transform(lambda x: x.clamp(min=0.1), dist.Normal(3, 5))

    def forward(model):
        model.mean = dist.Normal(0, 5)
        model.y = dist.Normal(model.mean, model.std)

    forward(model)
    train(model, forward=lambda: forward(model), epochs=200)
    npt.assert_allclose(float(model.posterior.mean.loc), loc, rtol=0.2)
    npt.assert_allclose(float(model.std.posterior.param.loc), scale, rtol=0.2)


def test_infer_with_paramaters():
    loc, scale = 2, 2
    data = torch.empty(2000).normal_(loc, scale)
    model = borch.Module()
    model.observe(y=data)
    model.loc = Parameter(torch.tensor(0.1))
    model.scale = Parameter(torch.tensor(0.1))

    def forward(model):
        model.y = dist.Normal(model.loc, model.scale.abs())

    forward(model)
    train(model, forward=lambda: forward(model), epochs=200)
    npt.assert_allclose(float(model.loc), loc, rtol=0.2)
    npt.assert_allclose(float(model.scale), loc, rtol=0.2)
