import pytest
import torch

import borch
from borch import Graph, RandomVariable, as_tensor, validate_args


class MyRV(RandomVariable):
    distribution_cls = torch.distributions.Normal

    def __init__(self, loc=1, scale=1, validate_args=None):
        super().__init__(validate_args=validate_args)
        self.register_param_or_buffer("loc", loc)
        self.register_param_or_buffer("scale", scale)
        self()

    def _distribution(self):
        """Create the distribution."""
        return self.distribution_cls(as_tensor(self.loc), as_tensor(self.scale))


@pytest.fixture
def rv():
    return MyRV()


def test__repr__(rv):
    str_repr = repr(rv)
    params = {**rv._buffers, **rv._modules, **rv._parameters}
    del params[borch.graph.TENSOR_ATTR]
    for key in params:
        assert key in str_repr
    assert "tensor" in str_repr


def test_base_random_variable_not_implemented_distribution():
    with pytest.raises(NotImplementedError):
        RandomVariable().distribution


def test_base_random_variable_not_implemented_distribution_cls():
    with pytest.raises(NotImplementedError):
        RandomVariable().distribution_cls()


def test_isinstance_random_variable(rv):
    assert isinstance(rv, RandomVariable)


def test_isinstance_graph(rv):
    assert isinstance(rv, Graph)


@pytest.mark.parametrize(
    "method",
    [
        "forward",
        "sample",
        "rsample",
        "log_prob",
        "cdf",
        "icdf",
        "perplexity",
    ],
)
def test_can_call_method(rv, method):
    assert getattr(rv, method)() is not None


@pytest.mark.parametrize(
    "prop",
    ["support", "has_rsample", "has_enumerate_support", "distribution"],
)
def test_can_access_property(rv, prop):
    assert getattr(rv, prop) is not None


def test_validate_args(rv):
    assert rv.validate_args


def test_validate_args_ctxt(rv):
    with validate_args(True):
        assert rv.validate_args
        with validate_args(False):
            assert not rv.validate_args
            with validate_args(True):
                assert rv.validate_args


def test_invalid_validate_arguments():
    with pytest.raises(ValueError):
        MyRV(validate_args="hello")


def test_validate_args_setter(rv):
    assert rv.validate_args
    rv.validate_args = False
    assert not rv.validate_args
