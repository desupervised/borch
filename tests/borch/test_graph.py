import numpy as np
import pytest
import torch

import borch
from borch import distributions as dist
from borch import graph


class RegisterWithoutForward(graph.Graph):
    def __init__(self, val):
        super().__init__()
        self.register_param_or_buffer("val", val)

    def forward(self):
        return self.val


class Register(RegisterWithoutForward):
    def __init__(self, val):
        super().__init__(val=val)
        self()


def test_graph_register_float_becomes_tensor():
    val = 1.0
    reg = Register(val)
    assert val == torch.tensor(reg.val)
    assert "val" in reg._buffers
    assert "val" not in reg._modules
    assert "val" not in reg._parameters


def test_graph_register_tensor():
    val = torch.randn(1)
    reg = Register(val)
    assert val == reg.val
    assert "val" in reg._buffers
    assert "val" not in reg._modules
    assert "val" not in reg._parameters


def test_graph_register_parameter():
    val = torch.nn.Parameter(torch.randn(1))
    reg = Register(val)
    assert val == reg.val
    assert "val" not in reg._buffers
    assert "val" not in reg._modules
    assert "val" in reg._parameters


def test_with_rv():
    reg = Register(dist.Normal(0, 1))
    assert isinstance(reg.tensor, torch.Tensor)
    assert isinstance(reg.val, torch.Tensor)
    rvs = list(borch.random_variables(reg))
    assert len(rvs) == 1
    assert isinstance(rvs[0], dist.Normal)


def test_inherits_borch_module():
    assert isinstance(Register(1.0), borch.Module)


@pytest.mark.parametrize(("typ", "val"), [(float, 1.0), (int, 1)])
def test_float(typ, val):
    out = typ(Register(val))
    assert out == val
    assert isinstance(out, typ)


@pytest.mark.parametrize(
    ("fn", "inpt", "output"),
    [
        (torch.exp, 0.0, torch.ones(1)),
        (torch.sinh, 0.0, torch.zeros(1)),
    ],
)
def test_torch_function_interface(fn, inpt, output):
    assert fn(Register(inpt)) == output


@pytest.mark.parametrize(
    "val",
    [
        torch.randn(2),
        1,
        1.0,
        np.random.randn(3),
        Register(1.0),
        RegisterWithoutForward(2.0),
    ],
)
def test_as_tensor(val):
    assert isinstance(graph.as_tensor(val), torch.Tensor)


@pytest.mark.parametrize(
    "val",
    [
        [
            torch.randn(2),
        ],
        [Register(1.0), RegisterWithoutForward(2.0)],
    ],
)
def test_as_tensor_list_with_tensors(val):
    out = graph.as_tensor(val)
    assert isinstance(out[0], torch.Tensor)
    assert isinstance(out, list)


def test_overloaded_mul():
    assert float(Register(1.0) * 3) == 3


def test_calling_tensor_instanciate_buffer():
    val = RegisterWithoutForward(2.0)
    assert val._tensor.nelement() == 0
    assert val.tensor == 2
    assert val._tensor == 2


def test_adding_float_instanciate_buffer():
    val = RegisterWithoutForward(2.0)
    assert val._tensor.nelement() == 0
    assert val + 2 == 4
    assert val._tensor == 2


def test_fn_exp_instanciate_buffer():
    val = RegisterWithoutForward(2.0)
    assert val._tensor.nelement() == 0
    assert torch.exp(val) > 4
    assert val._tensor == 2


def test_empty_graph():
    mod = borch.nn.Linear(2, 1)
    assert sum([val.nelement() == 0 for val in mod.buffers()]) == 0
    mod.apply(graph.empty_graph)
    assert sum([val.nelement() == 0 for val in mod.buffers()]) == 6
