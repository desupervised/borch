import pickle
import random

import pytest
import torch

import borch
from borch import distributions as dist
from borch.posterior import automatic


class Model(borch.Module):
    def __init__(self):
        super().__init__(posterior=automatic.Automatic())
        self.rv = dist.Normal(0, 1)
        self.nested = dist.Normal(dist.Cauchy(0, 1), dist.LogNormal(2, 3))

    def forward(self):
        x = torch.randn(random.randint(1, 100), requires_grad=True)
        self.dynamic_size = dist.Normal(x + 1, x.exp())
        return self.dynamic_size


@pytest.fixture
def model():
    mdl = Model()
    borch.sample(mdl)
    return mdl


def test_pickle(model):
    pickle.dumps(model)
    pickle.dumps(model.posterior)
    pickle.dumps(model.prior)


def test_warn_of_no_grad_context():
    with pytest.warns(UserWarning), torch.no_grad():
        Model()


def test_posteror_rv_requires_grad(model):
    assert model.posterior.rv.requires_grad
    assert model.rv.requires_grad


def test_nested_random_variable(model):
    assert isinstance(model.nested, torch.Tensor)
    assert len(list(borch.random_variables(model.posterior.nested))) == 1
    assert len(list(model.posterior.nested.buffers())) == 3


def test_can_create_dynamic_sized_variables(model):
    shapes = []

    def forward():
        borch.sample(model)
        return model()

    shapes = [model().shape for _ in range(10)]
    assert len(set(shapes)) > 1


def test_handles_transformed_distributions_correctly():

    module = borch.Module(posterior=automatic.Automatic())
    module.rv = dist.TransformedDistribution(
        dist.Normal(0, 1), torch.distributions.ExpTransform(),
    )
    assert module.rv.grad_fn is not None
