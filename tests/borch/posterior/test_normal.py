import torch

from borch import distributions as dist
from borch.posterior import normal


def test_Normal_with_cauchy_rv():
    rv = dist.Cauchy(torch.ones(2, 3), 1)
    post = normal.Normal(-2)
    post.register_random_variable("test", rv)
    assert isinstance(post.test, dist.Normal)
    assert float(post.test.get("scale").param.mean()) == -2
    assert list(post.test.get("loc").shape) == [2, 3]


def test_Normal_with_gamma():
    rv = dist.Gamma(torch.ones(3), torch.ones(3))
    post = normal.Normal(-2)
    post.register_random_variable("test", rv)
    assert isinstance(post.test, dist.TransformedDistribution)
    assert float(post.test.get("base_distribution").get("scale").param.mean()) == -2
    assert list(post.test.get("base_distribution").get("scale").shape) == [3]
