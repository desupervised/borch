import pytest
import torch

import borch
from borch import distributions as dist
from borch.posterior import manual


class Model(borch.Module):
    def __init__(self):
        super().__init__(posterior=manual.Manual())
        self.rv = borch.RVPair(
            dist.Normal(0, 1), dist.Normal(torch.nn.Parameter(torch.ones(1)), 1),
        )
        self.posterior.rv2 = dist.Normal(torch.nn.Parameter(torch.ones(1)), 1)
        self.rv2 = dist.Normal(0, 1)

    def forward(self):
        return self.rv + self.rv2


def test_corrcet_number_of_paramaters():
    assert len(list(Model().parameters())) == 2


def test_trying_to_auto_add_will_raise_warning():
    model = Model()
    with pytest.raises(RuntimeError):
        model.new_var = dist.Normal(0, 1)
