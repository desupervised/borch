import pickle
from io import BytesIO

import pytest
import torch
from torch.nn import Parameter

import borch
from borch import RVPair, nn, posterior
from borch import distributions as dist
from borch.distributions import Normal


def _post_to_test():
    return [
        add_normal_rv_(posterior.Normal()),
        add_normal_rv_(posterior.Automatic()),
        add_normal_rv_(posterior.AsPrior()),
    ]


def add_normal_rv_(post):
    par = Parameter(torch.ones(3))
    dist = Normal(par, 1)
    post.register_random_variable("test", dist)
    return post


@pytest.fixture(params=_post_to_test())
def post(request):
    return request.param


def test_can_be_pickled(post):
    f = BytesIO()
    pickle.dump(post, f)
    f.seek(0)
    pickle.load(f)


def test_call_double_proppagates(post):
    post.double()
    for par in post.parameters():
        assert par.dtype == torch.double


def _test_add_non_tensor(post, obj):
    torch.tensor(1.0)


def test_manual_add_tensor(post):
    _test_add_non_tensor(post, torch.ones(1))


def test_manual_add_parameter(post):
    _test_add_non_tensor(post, Parameter(torch.ones(1)))


def test_manual_add_str(post):
    _test_add_non_tensor(post, "hello")


def test_manual_add_int(post):
    _test_add_non_tensor(post, 1)


class Mul(nn.Module):
    def __init__(self, posterior=None):
        super().__init__(posterior=posterior)
        self.weight = Normal(1, 1)
        self.bias = RVPair(Normal(1, 1), Normal(Parameter(torch.tensor(0.5)), 1.0))

    def forward(self, x):
        return x * self.weight + self.bias


def test_construct_small_model(post):
    mul = Mul(posterior=post)
    out = mul(torch.ones(2))
    assert out.requires_grad
    assert post.weight is not None
    assert mul.bias.requires_grad


def test_train_loop(post):
    model = Mul(posterior=post)
    for _ in range(3):
        loss = 0
        model.zero_grad()
        for _ in range(3):
            borch.sample(model)
            model(torch.randn(3, 1))
            pqv = borch.pq_to_infer(model)
            loss += sum(
                p_dist.log_prob(val) - q_dist.log_prob(val)
                for p_dist, q_dist, val in zip(
                    pqv["p_dists"], pqv["q_dists"], pqv["values"], strict=False,
                )
            )
        loss.backward()


def test_assign_rv_in_forward(post):
    model = borch.Module(posterior=post)

    def _model(model):
        model.rv1 = dist.Normal(0, 1)
        model.rv2 = dist.Normal(0, 1)
        model.sigma = dist.LogNormal(1, 1)
        model.y = dist.Normal(model.rv1 + model.rv2, model.sigma)

    for _ in range(3):
        loss = 0
        model.zero_grad()
        for _ in range(3):
            borch.sample(model)
            _model(model)
            pqv = borch.pq_to_infer(model)
            loss += sum(
                p_dist.log_prob(val) - q_dist.log_prob(val)
                for p_dist, q_dist, val in zip(
                    pqv["p_dists"], pqv["q_dists"], pqv["values"], strict=False,
                )
            )
        loss.backward()


def test_forward_on_posterior_raises_error(post):
    with pytest.raises(RuntimeError):
        post()
