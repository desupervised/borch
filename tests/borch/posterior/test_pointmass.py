import pytest
import torch
from torch.distributions import constraints

import borch
from borch import distributions as dist
from borch.posterior import pointmass
from borch.utils.state_dict import add_state_dict_to_state, sample_state


class Model(borch.Module):
    def __init__(self):
        super().__init__(posterior=pointmass.PointMass())
        self.rv1 = dist.Normal(0, 1)
        self.rv2 = dist.Gamma(1, 1)

    def forward(self):
        return self.rv1 + self.rv2


def test_corrcet_number_of_paramaters():
    assert len(list(Model().parameters())) == 2


def test_save_state():
    model = Model()
    state = []
    for _ in range(3):
        borch.sample(model)
        model()
        for par in model.parameters():
            par.data += 1
        state = add_state_dict_to_state(model, state)
    assert len(state) == 3
    assert isinstance(state, list)
    assert isinstance(state[0], dict)
    return model, state


def test_sample_state():
    model, state = test_save_state()
    out = []
    for _ in range(10):
        sample_state(model, state)
        out.append(float(model()))
    assert len(set(out)) > 0


def test_register_q_random_variable_that_is_not_pointmass_fails():
    with pytest.raises(ValueError):
        pointmass.PointMass().register_q_random_variable(
            "fail", None, dist.Normal(0, 1),
        )


def test_register_q_random_variable_that_is_pointmass():
    rv = dist.PointMass(torch.nn.Parameter(torch.randn(1)), constraints.real)
    posterior = pointmass.PointMass()
    posterior.register_q_random_variable("works", None, rv)
    assert id(rv) == id(posterior.works)
