import copy
import pickle
from io import BytesIO
from unittest import mock

import pytest
import torch
from torch.nn import Parameter

from borch import Transform, module, posterior, sample
from borch import distributions as dist


class _Model(module.Module):
    def __init__(self):
        super().__init__()
        self.nested = module.Module()
        self.par = Parameter(torch.tensor(2.0))
        self.transformed_param = Transform(torch.exp, Parameter(torch.ones(3)))
        self.normal = dist.Normal(0, 1)
        self.nested.normal = dist.Normal(0, 1)

    def forward(self):
        return self.par + self.transformed_param + self.normal + self.nested.normal


class HierarchicalModel(module.Module):
    def __init__(self):
        super().__init__()
        self.transformed_param = Transform(torch.exp, Parameter(torch.ones(3)))
        self.normal = dist.Normal(0, 1)

    def forward(self):
        self.mu = dist.Normal(self.normal, self.transformed_param)
        self.scale = dist.Normal(self.normal, self.transformed_param)
        self.lik = dist.Normal(self.mu, self.scale.abs())
        return self.lik


class ReusedDistribution(module.Module):
    def __init__(self):
        super().__init__()
        normal = dist.Normal(0, 1)
        self.normalnormal1 = dist.Normal(normal, 1.0)
        self.normalnormal2 = dist.Normal(normal, 1.0)

    def forward(self):
        return self.normalnormal1 * self.normalnormal2


@pytest.fixture
def model():
    return _Model()


@pytest.mark.parametrize("fn", [copy.copy, copy.deepcopy])
def test_copy_resused_distributions(fn):
    model = HierarchicalModel()
    model()
    model_copy = fn(model)
    fn(torch.tensor([]))
    assert id(model_copy) != id(model)
    assert id(model.posterior.normal) != id(model_copy.posterior.normal)
    assert id(model.prior.normal) != id(model_copy.prior.normal)
    sdc = model_copy.state_dict()
    sd = model.state_dict()
    assert set(sdc) == set(sd)
    assert not {id(val) for val in sdc.values()}.intersection(
        {id(val) for val in sd.values()},
    )
    sample(model_copy, True, True)
    sample(model, True, True)
    model()
    model_copy()
    assert id(model.normal) == id(model.prior.mu.loc) == id(model.posterior.mu.loc)
    assert id(model.posterior.mu.loc) != id(model_copy.posterior.mu.loc)
    assert (
        id(model_copy.posterior.normal.tensor)
        == id(model_copy.prior.mu.loc)
        == id(model_copy.posterior.mu.loc)
    )


@pytest.mark.parametrize("fn", [copy.copy, copy.deepcopy])
def test_copy(model, fn):
    model_copy = fn(model)
    assert id(model_copy) != id(model)
    for key in ["nested", "par", "transformed_param", "normal", "prior", "posterior"]:
        assert id(getattr(model_copy, key)) != id(getattr(model, key))
    assert id(model.posterior.normal) != id(model_copy.posterior.normal)
    assert id(model.prior.normal) != id(model_copy.prior.normal)
    sdc = model_copy.state_dict()
    sd = model.state_dict()
    assert set(sdc) == set(sd)
    assert not {id(val) for val in sdc.values()}.intersection(
        {id(val) for val in sd.values()},
    )


def test_is_automatic_posterior():
    assert isinstance(module.Module().posterior, posterior.Automatic)


def test_observe(model):
    val = torch.tensor(2)
    model.observe(normal=val)
    assert model.normal == val
    return val


def test_unobserve(model):
    val = test_observe(model)
    module.unobserve(model)
    assert model.normal != val


def test_unobserve_with_missing_observed_rvs(model):
    model.observe(missing=torch.randn(2))
    module.unobserve(model)


def test_observe_is_part_of_state_dict(model):
    val = test_observe(model)
    assert model.state_dict()["observed.normal"] == val


def test_unobserve_normal_specifc(model):
    val = test_observe(model)
    model.observe(normal=None)
    assert model.normal != val


def test_unobserve_all(model):
    val = test_observe(model)
    model.observe(None)
    assert model.normal != val


def test_observe_invalid_type(model):
    with pytest.raises(TypeError):
        model.observe(test=1)


def test_observe_invalid_arg(model):
    with pytest.raises(ValueError):
        model.observe(1)


def test_setattr_before_init_fails():
    class Fails(module.Module):
        def __init__(self):
            self.rv = dist.Normal(0, 1)
            super().__init__()

    with pytest.raises(AttributeError):
        Fails()


def test_correct_state_dict(model):
    expected = {
        "prior.normal._tensor",
        "prior.normal.scale",
        "prior.normal.loc",
        "posterior.normal.loc",
        "posterior.normal.scale._tensor",
        "nested.posterior.normal.scale.param",
        "posterior.normal.scale.param",
        "nested.prior.normal._tensor",
        "transformed_param.param",
        "par",
        "nested.posterior.normal._tensor",
        "transformed_param._tensor",
        "nested.posterior.normal.scale._tensor",
        "posterior.normal._tensor",
        "nested.prior.normal.scale",
        "nested.prior.normal.loc",
        "nested.posterior.normal.loc",
    }
    assert set(model.state_dict().keys()) == expected


def test_can_be_pickled(model):
    f = BytesIO()
    pickle.dump(model, f)
    f.seek(0)
    pickle.load(f)


def test_call_double_proppagates(model):
    model.double()
    for par in model.parameters():
        assert par.dtype == torch.double


def test_get_rv_gives_only_tensor(model):
    assert isinstance(model.normal, torch.Tensor)


def test_can_remove_posterior_by_setting_it_to_none(model):
    model.posterior = None
    assert model._modules["posterior"] is None


def test_set_rv_without_posterior_gives_runtime_error(model):
    model.normal2 = dist.Normal(1, 1)


def test_assign_rv_in_forward():
    model = module.Module()

    def _model(model):
        model.rv1 = dist.Normal(0, 1)
        model.rv2 = dist.Normal(0, 1)
        model.sigma = dist.LogNormal(1, 1)
        model.y = dist.Normal(model.rv1 + model.rv2, model.sigma)

    for _ in range(3):
        loss = 0
        model.zero_grad()
        for _ in range(3):
            sample(model)
            _model(model)
            pqv = module.pq_to_infer(model)
            loss += sum(
                p_dist.log_prob(val) - q_dist.log_prob(val)
                for p_dist, q_dist, val in zip(
                    pqv["p_dists"], pqv["q_dists"], pqv["values"], strict=False,
                )
            )
        loss.backward()


def test_sample_creates_new_tensors(model):
    ids = (id(model.normal), id(model.nested.normal))
    module.sample(model)
    assert ids != (id(model.normal), id(model.nested.normal))


def test_sample_update_to_new_values(model):
    vals = (model.normal.clone(), model.nested.normal.clone())
    module.sample(model)
    assert vals != (model.normal, model.nested.normal)


def test_sample_resets_used_rvs(model):
    model.normal
    assert model._used_rvs
    module.sample(model)
    assert not model._used_rvs


def test_sample_occurs_only_once_per_module():
    model = ReusedDistribution()
    mock_forward = mock.Mock(return_value=torch.randn(1))
    model.prior.normalnormal1.forward = mock_forward
    module.sample(model, True, True)
    mock_forward.assert_called_once()


def test_getattr_updates_used_rvs(model):
    model._used_rvs.clear()
    model.normal
    assert "normal" in model._used_rvs


def test_pq_dict(model):
    p_q = module.pq_dict(model)
    assert len(p_q) == 2
    assert isinstance(p_q[0], dict)
    assert set(p_q[0]) == {"value", "posterior", "observed", "prior"}


def test_pq_to_infer(model):
    p_q = module.pq_to_infer(model)
    assert isinstance(p_q, dict)
    assert set(p_q) == {"q_dists", "p_dists", "observed", "values"}
    for val in p_q.values():
        assert len(val) == 2


class Inheritor(module.Module):
    def __init__(self):
        super().__init__(posterior=posterior.Automatic())
        mu = torch.tensor([1, 2, 3.0], requires_grad=True)
        sd = torch.tensor([1, 1, 1.0], requires_grad=True)
        self.rv1 = dist.Cauchy(mu, sd)

    def forward(self, *inputs):
        pass


def test_can_be_used_as_an_super_class():
    Inheritor()


def test_sample_only_posterior(model):
    before_prior = float(model.prior.normal.tensor)
    before_posterior = float(model.posterior.normal.tensor)
    module.sample(model, prior=False, posterior=True)
    assert before_prior == float(model.prior.normal.tensor)
    assert before_posterior != float(model.posterior.normal.tensor)


def test_sample_only_prior(model):
    before_prior = float(model.prior.normal.tensor)
    before_posterior = float(model.posterior.normal.tensor)
    module.sample(model, prior=True, posterior=False)
    assert before_prior != float(model.prior.normal.tensor)
    assert before_posterior == float(model.posterior.normal.tensor)


def test_sample_prior_and_posterior(model):
    before_prior = float(model.prior.normal.tensor)
    before_posterior = float(model.posterior.normal.tensor)
    module.sample(model, prior=True, posterior=True)
    assert before_prior != float(model.prior.normal.tensor)
    assert before_posterior != float(model.posterior.normal.tensor)


def test_named_random_variables(model):
    assert {"nested.posterior.normal", "posterior.normal"} == {
        name
        for name, _ in module.named_random_variables(model, prior=False, posterior=True)
    }


def test_set_posteriors(model):
    model.apply(module.set_posteriors(posterior.PointMass))
    assert isinstance(model.posterior, posterior.PointMass)
    assert isinstance(model.nested.posterior, posterior.PointMass)


def test_readds_rvs_to_posterior_if_missing(model):
    model.posterior = posterior.Automatic()
    assert not hasattr(model.posterior, "normal")
    model()
    assert hasattr(model.posterior, "normal")


@pytest.fixture
def observed():
    observed = module.Observed()
    observed["var1"] = torch.tensor(1.0)
    observed.var2 = torch.tensor(2.0)
    return observed


def test_observed_pop(observed):
    var1 = observed.pop("var1")
    assert "var1" not in observed
    assert var1 == torch.tensor(1.0)


def test_observed_del(observed):
    del observed["var1"]
    assert "var1" not in observed
    assert "var2" in observed
    observed.delete("var2")
    assert "var2" not in observed


def test_forward_on_observed_raises_error(observed):
    with pytest.raises(RuntimeError):
        observed()


def test_set_rv_overwrites_attr_in_dict():
    model = module.Module()
    model.test = 1
    assert model.__dict__["test"] == 1
    model.test = dist.Normal(0, 1)
    assert "test" not in model.__dict__
