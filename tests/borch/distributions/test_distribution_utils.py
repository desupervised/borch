import io
import pickle
from unittest import TestCase, mock

import numpy.testing as npt
import pytest
import torch
from torch import distributions as tdist
from torch.distributions import constraints, transforms
from torch.nn import Parameter

from borch import RandomVariable, Transform, distributions
from borch.distributions import distribution_utils as dist_utils


def _serilize_and_load(dist):
    with io.BytesIO() as bytes_io:
        pickle.dump(dist, bytes_io)
        bytes_io.seek(0)
        return pickle.load(bytes_io)


class Test_dist_to_qdist_infer_hierarchy_infer_hierarchy(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(loc=0, scale=torch.ones(1))
        self.q_dist = dist_utils.dist_to_qdist_infer_hierarchy(self.dist)

    def test_returns_a_borch_distribution(self):
        assert isinstance(self.q_dist, distributions.Normal)

    def test_returns_the_same_distribution_type(self):
        assert isinstance(self.q_dist, type(self.dist))

    def test_q_dist_arguments_are_transformed_parameters(self):
        assert isinstance(self.q_dist.loc, Parameter)
        assert isinstance(self.q_dist.get("scale"), Transform)

    def test_arguments_are_not_transformed_parameters_when_grad_required(self):
        dist = distributions.Normal(
            loc=torch.tensor([1.0], requires_grad=True),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist_infer_hierarchy(dist)
        assert not isinstance(q_dist.loc, Parameter)
        assert not isinstance(q_dist.get("scale"), Transform)

    def test_real_constraint_used_when_distribution_is_multinomal(self):
        # todo: I expect that this will fail...
        pass

    def test_can_be_pickled(self):
        dist = distributions.LogNormal(
            loc=torch.tensor([1.0], requires_grad=False),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist_infer_hierarchy(dist)
        loaded_dist = _serilize_and_load(q_dist)
        assert isinstance(loaded_dist, distributions.LogNormal)


class Test_dist_to_qdist(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(loc=0, scale=torch.ones(1))
        self.q_dist = dist_utils.dist_to_qdist(self.dist)

    def test_returns_a_borch_distribution(self):
        assert isinstance(self.q_dist, distributions.Normal)

    def test_returns_the_same_distribution_type(self):
        assert isinstance(self.q_dist, type(self.dist))

    def test_q_dist_arguments_are_transformed_parameters(self):
        q_dist = self.q_dist
        assert isinstance(q_dist.loc, torch.nn.Parameter)
        assert isinstance(q_dist.get("scale"), Transform)

    def test_transformed_parameters_when_grad_required_and_not_real_support(self):
        loc = torch.tensor([1.0], requires_grad=True)
        dist = distributions.Normal(
            loc=loc,
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist(dist)
        assert isinstance(q_dist.loc, torch.nn.Parameter)
        assert id(q_dist.loc) != id(loc)
        assert isinstance(q_dist._modules["scale"], Transform)

    def test_can_be_pickled_lognormal(self):
        dist = distributions.LogNormal(
            loc=torch.tensor([1.0], requires_grad=True),
            scale=torch.tensor([1.0], requires_grad=True),
        )
        q_dist = dist_utils.dist_to_qdist(dist)
        loaded_dist = _serilize_and_load(q_dist)
        assert isinstance(loaded_dist, distributions.LogNormal)

    def test_can_be_picklled(self):
        loaded_dist = _serilize_and_load(self.q_dist)
        assert isinstance(loaded_dist, distributions.Normal)


class Test_scaled_normal_dist_from_rv(TestCase):
    def setUp(self):
        self.scaling = 0.01
        self.rv = distributions.Cauchy(3, 3)
        self.new = dist_utils.scaled_normal_dist_from_rv(self.rv, self.scaling)

    def test_correct_values(self):
        assert self.rv.loc != float(self.new.loc)
        npt.assert_almost_equal(
            self.new.scale.detach(), self.scaling * self.rv.scale, decimal=2,
        )

    def test_arguments_are_transformed_parameters(self):
        for arg in ["loc", "scale"]:
            assert isinstance(self.new.get(arg), Transform | Parameter)

    def test_arguments_are_optimisable(self):
        params = list(self.new.parameters())
        for par in params:
            assert par.requires_grad
        assert len(params) == 2

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        self.rv.tensor /= 0
        with mock.patch.object(self.rv, "sample", wraps=self.rv.sample) as mocked:
            dist_utils.scaled_normal_dist_from_rv(self.rv, 0.01)
        mocked.assert_called_once()

    @staticmethod
    def test_half_normal():
        dist = distributions.HalfNormal(1)
        q_dist = dist_utils.scaled_normal_dist_from_rv(dist, 0.1)
        assert isinstance(q_dist, distributions.TransformedDistribution)

    @staticmethod
    def test_loc_at_mean_false():
        loc = 1
        dist = distributions.Normal(loc, 1)
        q_dist = dist_utils.scaled_normal_dist_from_rv(dist, 0.1, False)
        assert float(q_dist.loc) != loc

    @staticmethod
    def test_loc_at_mean_true():
        loc = 1
        dist = distributions.Normal(loc, 1)
        q_dist = dist_utils.scaled_normal_dist_from_rv(dist, 0.1, True)
        assert float(q_dist.loc) == loc


class TestGetConstraint(TestCase):
    def setUp(self):
        self.dist = distributions.Normal(1, 1)

    def test_returns_real_when_name_is_logits(self):
        ans = dist_utils._get_constraint("logits", self.dist)
        assert constraints.real is ans

    def test_returns_none_if_name_not_in_arg_constraints(self):
        assert dist_utils._get_constraint("x", self.dist) is None


class TestNormalDistributionFromRV(TestCase):
    def setUp(self):
        self.rv = distributions.Cauchy(3, 3)
        self.new = dist_utils.normal_distribution_from_rv(self.rv, -2)

    def test_correct_number_of_paramaters(self):
        assert len(list(self.new.parameters())) == 2

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        self.rv.tensor /= 0
        with mock.patch.object(self.rv, "sample", wraps=self.rv.sample) as mocked:
            dist_utils.normal_distribution_from_rv(self.rv, -2)
        mocked.assert_called_once()

    def test_gamma(self):
        rv = distributions.Gamma(1, 1)
        new = dist_utils.normal_distribution_from_rv(rv, -3.0)
        assert isinstance(new, distributions.TransformedDistribution)

    @staticmethod
    def test_loc_at_mean_false():
        loc = 1
        dist = distributions.Normal(loc, 1)
        q_dist = dist_utils.normal_distribution_from_rv(dist, 0.1, False)
        assert float(q_dist.loc) != loc

    @staticmethod
    def test_loc_at_mean_true():
        loc = 1
        dist = distributions.Normal(loc, 1)
        q_dist = dist_utils.normal_distribution_from_rv(dist, 0.1, True)
        assert float(q_dist.loc) == loc


class Test_delta_distribution_from_rv(TestCase):
    def setUp(self):
        self.rv = distributions.Cauchy(3, 3)

    def test_arguments_requires_grad(self):
        new = dist_utils.delta_distribution_from_rv(self.rv)
        assert new.value.requires_grad
        assert isinstance(new.value, Parameter)

    def test_non_finite_rv_value_causes_sample_of_rv(self):
        self.rv.tensor /= 0
        with mock.patch.object(self.rv, "sample", wraps=self.rv.sample) as mocked:
            dist_utils.delta_distribution_from_rv(self.rv)
        mocked.assert_called_once()

    def test_gamma(self):
        rv = distributions.Gamma(1, 1)
        new = dist_utils.delta_distribution_from_rv(rv)
        assert isinstance(new, distributions.TransformedDistribution)

    def test_gamma_optimizable_paramaters(self):
        rv = distributions.Gamma(1, 1)
        new = dist_utils.delta_distribution_from_rv(rv)
        assert len(list(new.parameters())) == 1


def test_requires_grad_float():
    assert not dist_utils.requires_grad(1.0)


def test_requires_grad_graph():
    assert dist_utils.requires_grad(
        distributions.Normal(Parameter(torch.tensor(1.0)), 1.0),
    )
    assert not dist_utils.requires_grad(distributions.Normal(1.0, 1.0))


def test_requires_str_raises_error():
    with pytest.raises(TypeError):
        dist_utils.requires_grad("hello")


def test_as_distribution_torch_dist():
    normal = tdist.Normal(0, 1)
    out = dist_utils.as_distribution(normal)
    assert id(normal) == id(out)
    assert isinstance(out, tdist.Distribution)


def test_as_distribution_rv():
    normal = distributions.Normal(0, 1)
    out = dist_utils.as_distribution(normal)
    assert id(normal) != id(out)
    assert isinstance(out, tdist.Distribution)


def test_get_init_kwargs():
    kwargs = dist_utils.get_init_kwargs(tdist.Normal(0, 1))
    assert set(kwargs) == {"loc", "scale"}


def test_is_continous_for_int_support_is_false():
    assert not dist_utils.is_continuous(tdist.constraints.positive_integer)
    assert not dist_utils.is_continuous(tdist.constraints.nonnegative_integer)
    assert not dist_utils.is_continuous(tdist.constraints.integer_interval(0, 1))


def test_is_continous_for_real():
    for support in [tdist.constraints.real, tdist.constraints.positive]:
        assert dist_utils.is_continuous(support)


def test__verify_is_continous():
    with pytest.raises(RuntimeError):
        dist_utils._verify_is_continuous(
            distributions.Categorical(logits=torch.randn(3)),
        )


@pytest.fixture(
    params=[
        distributions.Normal(torch.zeros(3, requires_grad=True), torch.ones(3)),
        distributions.Normal(
            torch.tensor(0.5, requires_grad=True),
            torch.tensor(1e-2, requires_grad=True),
        ),
        torch.distributions.Normal(torch.zeros(3), torch.ones(3, requires_grad=True)),
        distributions.Cauchy(torch.zeros(3, requires_grad=True), torch.ones(3)),
        distributions.Cauchy(torch.zeros(3), torch.ones(3, requires_grad=True)),
        distributions.Laplace(torch.zeros(3), torch.ones(3, requires_grad=True)),
        distributions.LogNormal(
            torch.tensor([0.0]), torch.tensor([1.0], requires_grad=True),
        ),
        distributions.Exponential(torch.tensor([1.0], requires_grad=True)),
        distributions.TransformedDistribution(
            distributions.Cauchy(0, torch.tensor(2.0, requires_grad=True)),
            torch.distributions.transforms.ExpTransform(),
        ),
    ],
)
def dist(request):
    return request.param


def test_rsample(dist):
    sample = dist.rsample()
    assert isinstance(sample, torch.Tensor)
    assert sample.grad_fn is not None


@pytest.mark.parametrize(
    "fn", [dist_utils.dist_to_qdist, dist_utils.dist_to_qdist_infer_hierarchy],
)
def test_qdist_creator(dist, fn):
    new = fn(dist)
    assert new.rsample().grad_fn is not None
    loss = new.log_prob(new.rsample()).sum()
    assert loss.grad_fn is not None
    loss.backward()
    assert isinstance(new, type(dist))
    if isinstance(dist, RandomVariable):
        for par in new.parameters():
            assert torch.isfinite(par.grad).all()


@pytest.mark.parametrize(
    "fn", [dist_utils.dist_to_qdist, dist_utils.dist_to_qdist_infer_hierarchy],
)
def test_qdist_creator_with_uknown_argument(fn):
    class NormalUnknownArg(distributions.Normal):
        def __init__(self, loc, scale, unknown):
            super().__init__(loc, scale)
            self.unknown = unknown

    val = torch.tensor(2.0)
    dist = NormalUnknownArg(1, 1, val)
    test_qdist_creator(dist, fn)
    qdist = fn(dist)
    assert id(qdist.unknown) != id(val)


def test_detach_dist(dist):
    detached = dist_utils.detach_dist(dist)
    assert detached.rsample().grad_fn is None
    assert detached.log_prob(detached.rsample()).sum().grad_fn is None


def test_scale_transformed_paramater():
    dist = distributions.TransformedDistribution(
        distributions.HalfNormal(1),
        [transforms.SigmoidTransform().inv, transforms.AffineTransform(loc=2, scale=5)],
    )
    npt.assert_allclose(dist_utils._scale(dist), 37.5, rtol=0.3)


def test_mean_transformed_paramater():
    dist = distributions.TransformedDistribution(
        distributions.HalfNormal(1),
        [transforms.SigmoidTransform().inv, transforms.AffineTransform(loc=2, scale=5)],
    )
    npt.assert_allclose(dist_utils._dist_mean(dist), 25.4, rtol=0.3)
