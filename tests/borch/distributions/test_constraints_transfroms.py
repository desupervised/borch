import pytest
import torch
from torch.distributions import constraints

from borch.distributions.constraint_transforms import transform


@pytest.mark.parametrize(
    "con", [constraints.real, constraints.positive, constraints.less_than(0)],
)
def test_transform(con):
    con.check(transform(con)(torch.randn(3, 3))).all()
