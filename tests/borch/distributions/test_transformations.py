"""tests."""

import unittest

import torch

from borch.distributions.transformations import IdentityTransform


class Test_IdentityTransform(unittest.TestCase):
    def test_identity_transform(self):
        val = torch.ones(1)
        assert IdentityTransform()(val) == val
        assert IdentityTransform().inv(val) == val


if __name__ == "__main__":
    unittest.main()
