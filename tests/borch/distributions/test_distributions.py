import io
import pickle
from unittest import TestCase

import numpy as np
import torch
from torch import distributions as dist
from torch.distributions import constraints

from borch.distributions import distributions


class DistBaseTest:
    # _distribution = distributions.Distribution
    _init_args = {}

    def setUp(self):
        self.dist = self._distribution(**self._init_args)

    def test_recreated_distribution_inherits_from_borch_distribution(self):
        assert isinstance(self.dist, dist.Distribution)

    def test_documentation_exists(self):
        assert len(self.dist.__doc__) > 100

    def test_pickle(self):
        with io.BytesIO() as bytes_io:
            pickle.dump(self.dist, bytes_io)
            bytes_io.seek(0)
            loaded_dist = pickle.load(bytes_io)
            assert isinstance(loaded_dist, self._distribution)


class TestPointMass(DistBaseTest, TestCase):
    _distribution = distributions.PointMass
    value = torch.randn(100, requires_grad=True)
    _init_args = {
        "value": value,
        "support": constraints.real,
        "event_shape": torch.Size([]),
    }

    def test_pickle(self):
        # current weakref issue
        pass

    def test_gradients_propagate_correctly(self):
        self.dist.rsample().sum().backward()
        assert self.value.grad is not None

    def test_support(self):
        assert isinstance(self.dist.support, type(constraints.real))

    def test_arg_constraints(self):
        assert isinstance(self.dist.arg_constraints, dict)
        assert len(self.dist.arg_constraints) == 1

    def test_init_with_scalar(self):
        pmd = distributions.PointMass(1, constraints.real)
        assert isinstance(pmd, distributions.PointMass)

    def test_log_prob_at_value(self):
        val = self.dist.rsample()
        assert torch.equal(self.dist.log_prob(val), torch.zeros(val.shape))

    def test_log_prob_at_value_at_differnt_val_then_value(self):
        val = self.dist.rsample() + 1
        assert torch.logical_not(torch.isfinite(self.dist.log_prob(val))).all()


class Test_Delta(DistBaseTest, TestCase):
    _distribution = distributions.Delta
    value = torch.randn(5, requires_grad=True)
    _init_args = {"value": value, "event_shape": torch.Size()}

    def test_gradients_propagate_correctly(self):
        self.dist.rsample().sum().backward()
        assert self.value.grad is not None

    def test_support(self):
        assert isinstance(self.dist.support, type(constraints.real))

    def test_arg_constraints(self):
        assert isinstance(self.dist.arg_constraints, dict)
        assert len(self.dist.arg_constraints) == 1

    def test_init_with_scalar(self):
        pmd = distributions.Delta(1, constraints.real)
        assert isinstance(pmd, dist.Distribution)

    def test_log_prob_returns_inf_if_summed(self):
        x = torch.ones(5)
        assert np.array(self.dist.log_prob(x).sum().abs()) == np.inf

    def test_log_prob_returns_zero_if_all_equal(self):
        x = torch.ones(5)
        test_dist = distributions.Delta(x)
        assert np.array(test_dist.log_prob(x).sum()) == 0

    def test_propper_vi_grad_elbo(self):
        par = torch.ones(5, requires_grad=True)
        q_dist = distributions.Delta(par)
        p_dist = dist.Normal(0, 1)
        x = q_dist.rsample()
        (p_dist.log_prob(x) - q_dist.log_prob(x)).sum().backward()
        assert float(par.grad.sum()) == -5

    def test_mean_returns_correct_value(self):
        test_dist = distributions.Delta(1)
        assert float(test_dist.mean) == 1

    def test_variance_returns_correct_value(self):
        test_dist = distributions.Delta(1)
        assert float(test_dist.variance) == 0

    def test_sample_does_not_require_grad(self):
        par = torch.ones(5, requires_grad=True)
        test_dist = distributions.Delta(par)
        assert not test_dist.sample().requires_grad
