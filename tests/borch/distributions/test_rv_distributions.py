import pytest
import torch

import borch
from borch.distributions import distribution_utils, rv_distributions


@pytest.fixture(
    params=[
        rv_distributions.Normal(torch.zeros(3), torch.ones(3)),
        rv_distributions.Normal(3, 1.0),
        rv_distributions.Cauchy(torch.zeros(3), torch.ones(3)),
        rv_distributions.Cauchy(1, 1),
        rv_distributions.Cauchy(1, 1),
        rv_distributions.Bernoulli(torch.tensor([0.3])),
        rv_distributions.Beta(torch.tensor([0.5]), torch.tensor([0.5])),
        rv_distributions.Binomial(100, torch.tensor([0, 0.2, 0.8, 1])),
        rv_distributions.Categorical(torch.tensor([0.25, 0.25, 0.25, 0.25])),
        rv_distributions.Chi2(torch.tensor([1.0])),
        rv_distributions.Dirichlet(torch.tensor([0.5, 0.5])),
        rv_distributions.Exponential(torch.tensor([1.0])),
        rv_distributions.FisherSnedecor(torch.tensor([1.0]), torch.tensor([2.0])),
        rv_distributions.Gamma(torch.tensor([1.0]), torch.tensor([1.0])),
        rv_distributions.Geometric(torch.tensor([0.3])),
        rv_distributions.HalfCauchy(torch.tensor([1.0])),
        rv_distributions.HalfNormal(torch.tensor([1.0])),
        rv_distributions.Kumaraswamy(torch.Tensor([1.0]), torch.Tensor([1.0])),
        rv_distributions.LKJCholesky(3, 0.5),
        rv_distributions.Laplace(torch.tensor([0.0]), torch.tensor([1.0])),
        rv_distributions.LogNormal(torch.tensor([0.0]), torch.tensor([1.0])),
        rv_distributions.Multinomial(1, logits=torch.tensor([1.0, 1.0, 1.0, 1.0])),
        rv_distributions.MultivariateNormal(torch.zeros(2), torch.eye(2)),
        rv_distributions.NegativeBinomial(100, logits=torch.tensor([0, 0.2, 0.8, 1])),
        rv_distributions.OneHotCategorical(torch.tensor([0.25, 0.25, 0.25, 0.25])),
        rv_distributions.Pareto(torch.tensor([1.0]), torch.tensor([1.0])),
        rv_distributions.Poisson(torch.tensor([4.0])),
        rv_distributions.RelaxedBernoulli(
            torch.tensor([2.2]), torch.tensor([0.1, 0.2, 0.3, 0.99]),
        ),
        rv_distributions.RelaxedOneHotCategorical(
            torch.tensor([2.2]), torch.tensor([0.1, 0.2, 0.3, 0.4]),
        ),
        rv_distributions.StudentT(torch.tensor([2.0]), 1, 2),
        rv_distributions.Uniform(torch.tensor([0.0]), torch.tensor([5.0])),
        rv_distributions.VonMises(torch.tensor([1.0]), torch.tensor([1.0])),
        rv_distributions.Weibull(torch.tensor([1.0]), torch.tensor([1.0])),
    ],
)
def dist(request):
    return request.param


def test_distribution_returns_torch_distribution(dist):
    assert isinstance(dist.distribution, torch.distributions.Distribution)


def test_log_prob_return_same_as_dist(dist):
    assert dist.log_prob().sum() == dist.distribution.log_prob(dist.tensor).sum()


@pytest.mark.parametrize("method", ["log_prob", "icdf", "cdf"])
def test_method_inserts_value_correctly(dist, method):
    try:
        val = getattr(dist, method)().sum()
        if torch.isfinite(val):
            assert val == getattr(dist.distribution, method)(dist.tensor).sum()
    except NotImplementedError:
        pass


@pytest.mark.parametrize("method", ["entropy", "perplexity"])
def test_method_(dist, method):
    try:
        val = getattr(dist, method)().sum()
        if torch.isfinite(val):
            assert val == getattr(dist.distribution, method)().sum()
    except NotImplementedError:
        pass


def test_sample_returns_tensor(dist):
    assert isinstance(dist.sample(), torch.Tensor)


def test_rsample_returns_tensor(dist):
    if dist.has_rsample:
        assert isinstance(dist.rsample(), torch.Tensor)


def test_bernoulli_with_both_probs_and_logits_fail():
    with pytest.raises(ValueError):
        rv_distributions.Bernoulli(torch.tensor([0.3]), torch.tensor([0.3]))


def test_inherits_borch_module(dist):
    assert isinstance(dist, borch.Module)


def test_inherits_graph(dist):
    assert isinstance(dist, borch.Graph)


def test_can_nest_rvs():
    rv = rv_distributions.Normal(
        rv_distributions.Cauchy(0, 1), rv_distributions.LogNormal(2, 3),
    )
    assert len(list(borch.random_variables(rv, prior=True, posterior=True))) == 5
    assert len(list(borch.random_variables(rv, prior=False, posterior=True))) == 3
    assert len(list(borch.random_variables(rv, prior=True, posterior=False))) == 3


def test_can_set_posterior(dist):
    kwargs = distribution_utils.get_init_kwargs(dist)
    kwargs["posterior"] = borch.posterior.ScaledNormal()
    new_dist = type(dist)(**kwargs)
    assert isinstance(new_dist.posterior, borch.posterior.ScaledNormal)


def test_validate_args_ctxt(dist):
    with borch.validate_args(True):
        assert dist.validate_args
        with borch.validate_args(False):
            assert not dist.validate_args
