import pickle as pkl
from io import BytesIO
from tempfile import NamedTemporaryFile
from unittest import TestCase, mock

import pytest
import torch
from torch.nn import Parameter

import borch
from borch import (
    Module,
    RandomVariable,
    Transform,
    distributions,
    posterior,
    pq_to_infer,
    rv_factories,
)
from borch.infer import vi_loss
from borch.nn import torch_proxies


class TestLinear(TestCase):
    in_features = 3
    out_features = 3
    _cls = torch_proxies.Linear
    _init_args = (in_features, out_features)
    _init_kwargs = {}

    def setUp(self):
        self.module = self._cls(*self._init_args, **self._init_kwargs)
        self.mu = Transform(lambda x: x, Parameter(torch.Tensor([1, 2, 3.0])))
        self.sd = Transform(lambda x: x, Parameter(torch.Tensor([1, 1, 1.0])))
        self.rv = distributions.Normal(self.mu, self.sd)

    def test_correct_scale_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.scale.mean().item(), 0.57735, delta=0.0001,
        )

    def test_is_normal_posterior(self):
        assert isinstance(self.module.posterior, posterior.Normal)

    def test_setting_prior_for_weight_and_bias_large_mean(self):
        module = torch_proxies.Linear(
            5,
            5,
            weight=distributions.Normal(100, 0.1),
            bias=distributions.Normal(100, 0.1),
        )
        assert module.prior.weight.mean() > 99
        assert module.prior.bias.mean() > 99

    def test_setting_prior_gamma_mean(self):
        module = torch_proxies.Linear(40, 40, weight=distributions.Gamma(1, 1))
        self.assertAlmostEqual(float(module.prior.weight.mean()), 1, delta=0.1)

    def test_setting_prior_gamma_forward(self):
        module = torch_proxies.Linear(
            2, 2, bias=False, weight=distributions.Gamma(1, 1),
        )
        assert module(torch.randn(1, 2).abs()).sum() > 0

    def test_setting_prior_categorical_forward(self):
        module = torch_proxies.Linear(
            2,
            2,
            bias=False,
            weight=distributions.Categorical(logits=torch.randn(2, 2, 2)),
            posterior=posterior.Automatic(),
        )
        assert module(torch.randn(1, 2).abs().long()).sum() >= 0

    def test_setting_prior_with_non_valid_brodcast_shape_raises_error(self):
        with pytest.raises(RuntimeError):
            torch_proxies.Linear(
                2,
                2,
                bias=False,
                weight=distributions.Categorical(logits=torch.randn(2, 2)),
                posterior=posterior.Automatic(),
            )

    def test_setting_prior_negative_binomial_forward(self):
        module = torch_proxies.Linear(
            2,
            2,
            bias=False,
            weight=distributions.NegativeBinomial(1, logits=2),
            posterior=posterior.Automatic(),
        )
        assert module(torch.randn(1, 2).abs()).sum() > 0

    def test_setting_prior_for_weight_large_std(self):
        module = torch_proxies.Linear(5, 5, weight=distributions.Normal(0, 100))
        assert module.prior.weight.std() > 50

    def test_setting_prior_for_big_conv2d(self):
        module = torch_proxies.Conv2d(
            256,
            256,
            kernel_size=(3, 3),
            stride=(2, 2),
            padding=(1, 1),
            weight=distributions.Normal(0, 100),
        )
        assert module.prior.weight.std() > 50

    def test_setting_prior_for_not_valid_name(self):
        with pytest.raises(ValueError):
            torch_proxies.Linear(5, 5, bad_name=distributions.Normal(0, 100))

    def test_correct_loc_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.loc.mean().item(), 0, delta=0.0001,
        )

    def test_weight_is_a_random_variable(self):
        assert isinstance(self.module.prior.weight, RandomVariable)
        assert isinstance(self.module.posterior.weight, RandomVariable)

    def test_bias_is_a_random_variable(self):
        assert isinstance(self.module.prior.bias, RandomVariable)
        assert isinstance(self.module.posterior.bias, RandomVariable)

    def test_borch_call_calls_torch_forward(self):
        # todo: generalise
        with mock.patch.object(torch.nn.Linear, "forward") as mocked:
            self.module(torch.ones(4, self.in_features))
        mocked.assert_called()

    def test_can_add_parameter_to_module(self):
        name = "param"
        setattr(self.module, name, torch.nn.Parameter(torch.ones(3)))
        assert isinstance(getattr(self.module, name), torch.nn.Parameter)

    def test_zero_grad_zeroes_gradients_of_all_parameters(self):
        self.module(torch.ones(4, self.in_features)).sum().backward()
        self.module.zero_grad()
        for par in self.module.parameters():
            if par.grad is not None:
                assert torch.equal(par.grad, torch.zeros_like(par.grad))

    def test_correct_number_optimisable_tensors(self):
        assert len(list(self.module.parameters())) == 4

    def test_gradients_are_none_before_backward_call(self):
        for param in self.module.parameters():
            assert param.grad is None

    def test_gradients_propagate_with_sampling_on(self):
        self.module(torch.ones(4, self.in_features)).sum().backward()
        for param in self.module.parameters():
            assert param.grad is not None
        assert len(list(self.module.parameters())) == 4

    def test_network_can_be_propagated_after_casting_to_double(self):
        img = torch.randn(self.in_features)
        self.module.double()
        self.module(img.double())

    def test_borch_linear_contains_twice_the_params_as_original(self):
        linear = torch.nn.Linear(self.in_features, self.out_features)
        orig_params = tuple(linear.parameters())
        new_params = tuple(self.module.parameters())
        assert len(new_params) == 2 * len(orig_params)

    def test_setattr_parameter_adds_a_new_parameter(self):
        param = torch.nn.Parameter(torch.ones(3))
        self.module.new_param = param
        assert self.module.new_param is param

    def test_parameter_can_be_registered_using_register_parameter(self):
        param = torch.nn.Parameter(torch.ones(1))
        # Note that by using `register_parameter` directly, `param` is not
        # added as a random variable.
        self.module.register_parameter("test", param)
        assert id(param) in map(id, self.module.parameters())

    def test_can_be_saved(self):
        with NamedTemporaryFile("wb") as f:
            sd = torch_proxies.Linear(self.in_features, self.out_features).state_dict()
            pkl.dump(sd, f)


class TestConv2d(TestCase):
    def setUp(self):
        self.module = torch_proxies.Conv2d(1, 1, 3, 1)

    def test_network_can_be_propagated_after_casting_to_double(self):
        img = torch.randn((2, 1, 28, 28))
        self.module.double()
        self.module(img.double())

    def test_correct_scale_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.scale.mean().item(), 0.333333, delta=0.0001,
        )

    def test_correct_loc_of_prior(self):
        self.assertAlmostEqual(
            self.module.prior.weight.loc.mean().item(), 0, delta=0.0001,
        )


class Test_BatchNorm2d(TestCase):
    def setUp(self):
        self.module = torch_proxies.BatchNorm2d(3)

    def test_no_rvs_for_batch_norm(self):
        for par in self.module.parameters():
            assert not isinstance(par, RandomVariable)


class Net(Module):
    def __init__(self):
        super().__init__()
        self.fc1 = torch_proxies.Linear(3, 3)
        self.fc2 = torch_proxies.Linear(3, 3)

    def forward(self, *inputs):
        return torch.sigmoid(self.fc2(torch.sigmoid(self.fc1(inputs[0]))))


class TestFullNetwork(TestCase):
    # todo: this is an integration test, move when appropriate
    _cls = Net

    def setUp(self):
        self.net = self._cls()
        self.net(torch.randn(2, 3))

    def test_gradients_propagate_for_entire_network(self):
        loss = vi_loss(**pq_to_infer(self.net))
        loss.backward()
        for param in self.net.parameters():
            assert param.grad is not None

    def test_correct_number_of_paramaters(self):
        assert len(list(self.net.parameters())) == 8

    def test_network_can_be_serialised(self):
        self.net(torch.randn(2, 3))
        f = BytesIO()
        torch.save(self.net.state_dict(), f)
        f.seek(0)
        net2 = Net()
        net2.load_state_dict(torch.load(f))
        assert torch.equal(net2.fc1.weight, self.net.fc1.weight)
        assert torch.equal(net2.fc1.posterior.weight, self.net.fc1.posterior.weight)


class Test_get_rv_factory(TestCase):
    def test_returns_callable(self):
        rv_factory = torch_proxies.get_rv_factory("Module without default posterior")
        assert callable(rv_factory)

    def test_returns_correct_rv_factory_for_linear(self):
        rv_factory = torch_proxies.get_rv_factory("Linear")
        assert rv_factory == rv_factories.kaiming_normal_rv


def test_sequential():
    net = borch.nn.Sequential(
        torch_proxies.Linear(3, 2), torch_proxies.ReLU(), torch_proxies.Linear(2, 1),
    )
    assert isinstance(net(torch.ones(1, 3)), torch.Tensor)
    loss = vi_loss(**pq_to_infer(net))
    assert loss > 0


def test_transformer_samples():
    transformer_model = borch.nn.Transformer(nhead=16, num_encoder_layers=12)
    src = torch.rand((10, 32, 512))
    tgt = torch.rand((20, 32, 512))
    out = transformer_model(src, tgt).sum()
    borch.sample(transformer_model)
    assert out != transformer_model(src, tgt).sum()


def test_transformer_submodules_are_borch_modules():
    transformer_model = borch.nn.Transformer(nhead=2, num_encoder_layers=2)
    for mod in transformer_model.children():
        if mod not in transformer_model.internal_modules:
            assert isinstance(mod, borch.Module)


@pytest.mark.parametrize(
    ("rnn", "state"),
    [
        (borch.nn.LSTMCell(10, 20), (torch.randn(3, 20), torch.randn(3, 20))),
        (borch.nn.GRUCell(10, 20), torch.randn(3, 20)),
        (borch.nn.RNNCell(10, 20), torch.randn(3, 20)),
    ],
)
def test_rnn_sum(rnn, state):
    inpt = torch.randn(5, 3, 10)
    assert len(list(rnn.parameters())) > 4
    for _ in range(4):
        rnn.zero_grad()
        borch.sample(rnn)
        output = 0
        for data in inpt:
            output += sum(rnn(data, state)).sum()
        output.sum().backward()
        assert [x.grad.abs().sum() > 0 != None for x in rnn.parameters()]


@pytest.fixture(
    params=[
        (borch.nn.Linear(2, 3), torch.randn(3, 2)),
        (borch.nn.Conv1d(6, 33, 3, stride=2), torch.randn(2, 6, 10)),
        (borch.nn.Conv2d(6, 33, 3, stride=2), torch.randn(2, 6, 10, 10)),
        (borch.nn.Conv3d(2, 33, 3, stride=2), torch.randn(1, 2, 10, 10, 3)),
        (
            borch.nn.ConvTranspose2d(6, 33, (3, 5), stride=(2, 1), padding=(4, 2)),
            torch.randn(2, 6, 10, 10),
        ),
        (borch.nn.Bilinear(20, 30, 40), (torch.randn(128, 20), torch.randn(128, 30))),
        (borch.nn.GRU(10, 20, 2), (torch.randn(2, 3, 10), torch.randn(2, 3, 20))),
        (borch.nn.RNN(10, 20, 2), (torch.randn(2, 3, 10), torch.randn(2, 3, 20))),
        (
            borch.nn.LSTM(10, 20, 2),
            (torch.randn(2, 3, 10), (torch.randn(2, 3, 20), torch.randn(2, 3, 20))),
        ),
        (borch.nn.RNNCell(10, 20), (torch.randn(3, 10), torch.randn(3, 20))),
        (borch.nn.GRUCell(10, 20), (torch.randn(3, 10), torch.randn(3, 20))),
        (
            borch.nn.LSTMCell(10, 20),
            (torch.randn(3, 10), (torch.randn(3, 20), torch.randn(3, 20))),
        ),
        (
            borch.nn.EmbeddingBag(10, 3, mode="sum"),
            (
                torch.tensor([1, 2, 4, 5, 4, 3, 2, 9], dtype=torch.long),
                torch.tensor([0, 4], dtype=torch.long),
            ),
        ),
        (borch.nn.Embedding(3, 5, max_norm=True), torch.tensor([1, 2])),
    ],
)
def mod_ipt(request):
    return request.param


def _save_module(save, load, mod_ipt):
    mod, ipt = mod_ipt
    buffer = BytesIO()
    save(mod, buffer)
    buffer.seek(0)
    new = load(buffer)
    assert isinstance(new, borch.Module)
    assert isinstance(new, type(mod))
    return new, ipt


def _save_state_dict(save, load, mod_ipt):
    mod, ipt = mod_ipt
    buffer = BytesIO()
    save(mod.state_dict(), buffer)
    buffer.seek(0)
    sd = load(buffer)
    borch.sample(mod)
    _run_sum(mod, ipt)
    mod.load_state_dict(sd)
    return mod, ipt


def _torch_save_module(x):
    return _save_module(torch.save, torch.load, x)
def _pkl_save_module(x):
    return _save_module(pkl.dump, pkl.load, x)
def _torch_save_state_dict(x):
    return _save_state_dict(torch.save, torch.load, x)
def _pkl_save_state_dict(x):
    return _save_state_dict(pkl.dump, pkl.load, x)


def _sample(mod_ipt):
    mod, ipt = mod_ipt
    borch.sample(mod)
    return mod, ipt


@pytest.fixture(
    params=[
        lambda x: x,
        _torch_save_module,
        _torch_save_state_dict,
        _sample,
        lambda x: _sample(_torch_save_module(x)),
        _pkl_save_module,
        _pkl_save_state_dict,
        lambda x: _sample(_pkl_save_module(x)),
        lambda x: _sample(_pkl_save_state_dict(x)),
        lambda x: _torch_save_module(_sample(x)),
        lambda x: _sample(_torch_save_module(_sample(x))),
        lambda x: _sample(_torch_save_state_dict(x)),
        lambda x: _sample(_torch_save_state_dict(_sample(x))),
        lambda x: _torch_save_state_dict(_sample(x)),
    ],
)
def mod_inpt_transform(mod_ipt, request):
    fn = request.param
    return fn(mod_ipt)


def _run_sum(mod, ipt):
    out = mod(*ipt) if isinstance(ipt, tuple) else mod(ipt)
    if isinstance(out, tuple):
        out = out[0]
    return out.sum()


def test_samples_propperly(mod_inpt_transform):
    mod, ipt = mod_inpt_transform
    out = _run_sum(mod, ipt)
    borch.sample(mod)
    assert out != _run_sum(mod, ipt)


def test_gradients_propagate(mod_inpt_transform):
    mod, ipt = mod_inpt_transform
    for _ in range(5):
        mod.zero_grad()
        borch.sample(mod)
        out = _run_sum(mod, ipt)
        out.backward()
        assert all(par.grad.abs().sum() > 0 for par in mod.parameters())


def test_has_some_paramaters(mod_inpt_transform):
    mod, _ = mod_inpt_transform
    assert len(list(mod.parameters())) >= 2
