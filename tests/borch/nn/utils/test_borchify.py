from unittest import TestCase, mock

import numpy.testing as npt
import pytest
import torch

from borch import nn, sample
from borch.module import random_variables
from borch.nn import borchify
from borch.posterior import ScaledNormal
from borch.rv_factories import parameter_to_normal_rv


class TestBorchifyLinearModule(TestCase):
    _cls = torch.nn.Linear
    _in_features = 2
    _out_features = 2
    _init_args = (_in_features, _out_features)
    _init_kwargs = {}

    def setUp(self):
        self.module = self._cls(*self._init_args, **self._init_kwargs)
        self.borchified = borchify.borchify_module(self.module)

    def test_borchified_module_inherits_from_original_torch_module(self):
        assert isinstance(self.borchified, self._cls)

    def test_inherits_from_borch_module(self):
        assert isinstance(self.borchified, nn.Module)

    def test_borchified_object_is_unique(self):
        assert id(self.module) != id(self.borchified)

    def test_original_object_retains_id(self):
        id_before = id(self.module)
        borchify.borchify_module(self.module)
        id_after = id(self.module)
        assert id_before == id_after

    def test_borchified_object_has_random_variables(self):
        assert bool(tuple(random_variables(self.borchified)))

    def test_changes_in_module_dont_affect_borch_module(self):
        param = torch.nn.Parameter(torch.ones(1))
        self.module.register_parameter("test", param)
        assert id(param) in map(id, self.module.parameters())
        assert id(param) not in map(id, self.borchified.parameters())

    def test_changes_in_borch_module_dont_affect_module(self):
        param = torch.nn.Parameter(torch.ones(1))
        # Note that by using `register_parameter` directly, `param` is not
        # added as a random variable.
        self.borchified.register_parameter("test", param)
        assert id(param) in map(id, self.borchified.parameters())
        assert id(param) not in map(id, self.module.parameters())

    def test_calls_default_rv_factory_when_no_rv_factory_provided(self):
        with mock.patch.object(borchify, "default_rv_factory") as mocked:
            borchify.borchify_module(self.module)
        mocked.assert_called()

    def test_does_not_call_default_rv_factory_when_rv_factory_provided(self):
        with mock.patch.object(borchify, "default_rv_factory") as mocked:
            borchify.borchify_module(self.module, rv_factory=parameter_to_normal_rv)
        assert not mocked.called


class ExampleNetwork(torch.nn.Module):
    """A test network for checking borchification."""

    def __init__(self):
        super().__init__()
        linear = torch.nn.Linear(2, 2)
        linear.add_module("nested", torch.nn.Linear(3, 3))
        linear.add_module("nested2", torch.nn.Linear(4, 4))
        self.linear = linear
        self.conv = torch.nn.Conv1d(1, 1, 3)
        self.flatten = nn.Flatten()

    def forward(self, *inputs):
        return self.linear(inputs[0])


def test_borch_proxy_class_borchify_submodules():
    dont_proppagate = borchify.borch_proxy_class(ExampleNetwork)()
    proppagate = borchify.borch_proxy_class(ExampleNetwork, borchify_submodules=True)()
    assert not isinstance(dont_proppagate.conv, nn.Module)
    assert isinstance(proppagate.conv, nn.Module)


class TestBorchifyNetwork(TestCase):
    def setUp(self):
        self.net = ExampleNetwork()
        self.borchified = borchify.borchify_network(self.net)

    def test_base_level_was_borchified(self):
        assert isinstance(self.borchified, nn.Module)

    def test_linear_was_borchified(self):
        assert isinstance(self.borchified.linear, nn.Module)
        assert isinstance(self.borchified.conv, nn.Module)

    def test_nested_was_borchified(self):
        assert isinstance(self.borchified.linear.nested, nn.Module)

    def test_linear_has_random_variables(self):
        rvs = tuple(random_variables(self.borchified.linear))
        assert bool(rvs)

    def test_nested_has_random_variables(self):
        rvs = tuple(random_variables(self.borchified.linear.nested))
        assert bool(rvs)

    def test_nested2_has_random_variables(self):
        rvs = tuple(random_variables(self.borchified.linear.nested2))
        assert bool(rvs)

    def test_conv_has_random_variables(self):
        rvs = tuple(random_variables(self.borchified.conv))
        assert bool(rvs)

    def test_double_usage_handled_appropriately(self):
        # Here we add `self.linear` as a nested module on `self.conv`. We want
        # to see that `self.linear` and its nested module are NOT re-borchified
        # and added, but instead the already borchified versions in the cache
        # are used.
        self.net.conv.add_module("nested", self.net.linear)

        # assert the setup is correct
        assert self.net.conv.nested is self.net.linear
        assert self.net.conv.nested.nested is self.net.linear.nested

        borchified = borchify.borchify_network(self.net)
        assert borchified.conv.nested is borchified.linear
        assert borchified.conv.nested.nested is borchified.linear.nested

    def test_can_call_forward(self):
        # Only a function call test
        out = self.borchified(torch.rand(10, 2))
        assert isinstance(out, torch.Tensor)

    def test_gradients_propagate(self):
        out = self.borchified(torch.rand(10, 2))
        out.sum().backward()
        assert any(par.grad is not None for par in self.borchified.parameters())

    def test_posterior_is_set_correctly_with_supplied_posterior_creator(self):
        posterior_creator = mock.MagicMock(
            side_effect=lambda *args, **kwargs: ScaledNormal(),
        )
        new = borchify.borchify_network(self.net, posterior_creator=posterior_creator)

        # First check that the number of calls was correct, this should be
        # equal to the total number of modules in the network
        n_modules = len(tuple(self.net.modules()))
        assert posterior_creator.call_count == n_modules

        # Then we can check that all of the posteriors for these modules have been
        # assigned to the return value of the posterior_creator
        for mod in [
            new.linear,
            new.linear.nested,
            new.linear.nested2,
            new.conv,
            new.flatten,
        ]:
            if isinstance(mod, nn.Module):
                assert isinstance(mod.posterior, ScaledNormal)


def test_borchify_sequential():
    net = torch.nn.Sequential(
        torch.nn.Linear(2, 3), torch.nn.ReLU(), torch.nn.Linear(3, 1),
    )
    bnet = borchify.borchify_network(net)
    out = bnet(torch.randn(4, 2))
    assert isinstance(out, torch.Tensor)
    out.sum().backward()
    assert all(par.grad is not None for par in bnet.parameters())


def test_gradeints_proppagate():
    net = torch.nn.Conv2d(
        64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), groups=64, bias=False,
    )
    bnet = borchify.borchify_network(net)
    bnet(torch.randn(4, 64, 16, 16)).sum().backward()
    not_none = [par.grad is not None for par in bnet.parameters()]
    assert bnet.posterior.weight.loc.grad is not None
    assert sum(not_none) == 2


def test_borchify_a_borch_module():
    net = nn.Linear(2, 1)
    new = borchify.borchify_network(net, posterior_creator=lambda: ScaledNormal(1e-6))
    assert isinstance(new(torch.randn(1, 2)), torch.Tensor)
    assert isinstance(new.posterior, ScaledNormal)


class MultiplyOrDevide(torch.nn.Module):
    def __init__(self, multiply=False):
        super().__init__()
        self.multiply = multiply
        self.parameter = torch.nn.Parameter(torch.tensor([1, 2, 3.0]))

    def forward(self, x):
        if self.multiply:
            return self.parameter * x
        return self.parameter / x


def test_custom_module_multiply():
    mul = borchify.borchify_network(
        MultiplyOrDevide(True),
        posterior_creator=lambda: ScaledNormal(1e-2, loc_at_prior_mean=False),
    )
    assert mul.multiply
    x = torch.tensor([2.0])
    out = mul(x)
    sample(mul)
    assert all(out != mul(x))
    npt.assert_allclose(out.detach(), [2, 4, 6], rtol=0.1)


def test_custom_module_devide():
    devide = borchify.borchify_network(
        MultiplyOrDevide(False),
        posterior_creator=lambda: ScaledNormal(1e-2, loc_at_prior_mean=False),
    )
    assert not devide.multiply
    npt.assert_allclose(devide(torch.tensor(2.0)).detach(), [0.5, 1, 1.5], rtol=0.1)


def test_borchify_namespace_samples_correct():
    bnn = borchify.borchify_namespace(torch.nn)
    lin = bnn.Linear(1, 2)
    assert isinstance(lin, nn.Module)
    out = float(sum(lin(torch.ones(1))))
    sample(lin)
    assert out != float(sum(lin(torch.ones(1))))


def test_borchify_namespace_propagates_gradients():
    lin = borchify.borchify_namespace(torch.nn).Linear(1, 2)
    lin(torch.ones(1)).sum().backward()
    has_grad = [x.grad is not None for x in lin.parameters()]
    assert sum(has_grad) == 4


def test_borchify_namespace_cache_from_borch_registry():
    bnn = borchify.borchify_namespace(torch.nn)
    for cls in borchify.BORCHIFY_REGISTRY.registry.values():
        assert (
            id(cls) == id(getattr(bnn, cls.__name__)) == id(getattr(nn, cls.__name__))
        )


def test_borchify_namespace_propagates_gradients_lstm():
    rnn = borchify.borchify_namespace(torch.nn).LSTM(10, 20, 2)
    inpt = torch.randn(5, 3, 10)
    output, _ = rnn(inpt, (torch.randn(2, 3, 20), torch.randn(2, 3, 20)))
    output.sum().backward()
    has_grad = [x.grad is not None for x in rnn.parameters()]
    assert sum(has_grad) == 2 * len(list(torch.nn.LSTM(10, 20, 2).parameters()))


def test_registry_cant_register_twize():
    reg = borchify.Registry()
    reg.register({MultiplyOrDevide: MultiplyOrDevide})
    with pytest.raises(RuntimeError):
        reg.register({MultiplyOrDevide: ExampleNetwork})


def test_borchify_class_inhereting_from_sequential():
    class Net(torch.nn.Sequential):
        def __init__(self):
            layers = [torch.nn.Linear(1, 2)]
            super().__init__(*layers)

    with pytest.warns(UserWarning):
        net = borchify.borchify_network(Net())
    assert isinstance(net(torch.randn(10, 1)), torch.Tensor)
