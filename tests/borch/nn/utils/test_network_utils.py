from io import BytesIO
from unittest import TestCase, mock

import pytest
import torch
import torch.nn as tnn

import borch.nn as ann
from borch import distributions
from borch.nn import utils
from borch.nn.utils import network_utils
from borch.nn.utils.network_utils import _coerce_type


class TestStatic(TestCase):
    def test_change_requiers_grad(self):
        net = torch.nn.Linear(2, 2)
        network_utils.static(net)
        for par in net.parameters():
            assert not par.requires_grad


class Test_split_sequential_into_two(TestCase):
    def setUp(self):
        self.net = torch.nn.Sequential(*[torch.nn.ReLU() for _ in range(10)])

    def test_index_minus_one(self):
        _, net2 = utils.split_sequential_into_two(self.net, -1)
        assert len(list(net2.children())) == 1

    def test_index_five(self):
        net1, _ = utils.split_sequential_into_two(self.net, 5)
        assert len(list(net1.children())) == 6

    def test_not_sequential_net(self):
        self.assertRaises(
            ValueError, lambda: utils.split_sequential_into_two(torch.nn.ReLU(), 5),
        )

    def test_float_as_index(self):
        self.assertRaises(
            ValueError, lambda: utils.split_sequential_into_two(self.net, 5.0),
        )


class Test__coerce_type(TestCase):
    def test__coerce_type_error(self):
        def should_raise_error():
            return _coerce_type("hej", "test", 2)

        self.assertRaises(ValueError, should_raise_error)

    def test___coerce_type_tuple(self):
        test = (1, 2)
        assert id(test) == id(_coerce_type(test, "test", 2))


class TestCalculatePostConvolutionDims(TestCase):
    def test_28x28_pixel_input(self):
        convdim = utils.calculate_post_convolution_dims((28, 28), [9, 9], [1, 2])
        assert convdim == (6, 6)

    def test_different_kernel_sizes_strides_lengths_raises_value_error(self):
        with pytest.raises(ValueError):
            utils.calculate_post_convolution_dims((28, 28), [9, 9, 1], [1, 2])

    def test_float_input_dim_raises_value_error(self):
        with pytest.raises(ValueError):
            utils.calculate_post_convolution_dims(1.2, [9, 9, 1], [1, 2])

    def test_float_input_stide_raises_value_error(self):
        with pytest.raises(ValueError):
            utils.calculate_post_convolution_dims(
                (28, 28), [9, 9, 1], torch.tensor([1, 2]),
            )

    def test_raises_value_error_for_non_matching_dimensions(self):
        with pytest.raises(ValueError):
            utils.calculate_post_convolution_dims(
                dim=(28, 28), kernel_sizes=[(9, 9, 9)], strides=[(9, 9, 9)],
            )


def save_at_mock_path(net):
    bytes_io = BytesIO()
    with mock.patch("borch.nn.utils.network_utils.open") as mock_open:
        mock_open.return_value = mock.MagicMock(__enter__=lambda _: bytes_io)
        ann.utils.save_net_state(net, "irrelevant/file/path/net.pth")


def test_save_with_torch():
    net = tnn.Sequential(tnn.Linear(3, 3))
    save_at_mock_path(net)


def test_save_with_borch():
    net = ann.Sequential(ann.Linear(3, 3))
    save_at_mock_path(net)


def test_save_with_mix():
    net = ann.Sequential(ann.Linear(3, 3), tnn.Linear(3, 3), ann.Linear(3, 3))
    save_at_mock_path(net)


class Test_load_net_state(TestCase):
    def check_equals(self, net1, net2, x=torch.randn(3)):
        bytes_io = BytesIO()
        with mock.patch("borch.nn.utils.network_utils.open") as mock_open:
            mock_open.return_value = mock.MagicMock(__enter__=lambda _: bytes_io)
            assert net1(x).sum().data != net2(x).sum().data
            ann.utils.save_net_state(net1, "irrelevant/file/path/net.pth")
            bytes_io.seek(0)
            ann.utils.load_net_state(net2, "irrelevant/file/path/net.pth")
            assert net1(x).sum().data == net2(x).sum().data

    @staticmethod
    def save_and_load_at_mock_path(net1, net2):
        bytes_io = BytesIO()
        with mock.patch("borch.nn.utils.network_utils.open") as mock_open:
            mock_open.return_value = mock.MagicMock(__enter__=lambda _: bytes_io)
            ann.utils.save_net_state(net1, "irrelevant/file/path/net.pth")
            bytes_io.seek(0)
            ann.utils.load_net_state(net2, "irrelevant/file/path/net.pth")

    def test_with_torch(self):
        net1 = tnn.Sequential(tnn.Linear(3, 3))
        net2 = tnn.Sequential(tnn.Linear(3, 3))
        self.check_equals(net1, net2)

    def test_with_borch(self):
        net1 = ann.Sequential(ann.Linear(3, 3))
        net2 = ann.Sequential(ann.Linear(3, 3))
        self.check_equals(net1, net2)

    def test_with_mix(self):
        net1 = ann.Sequential(ann.Linear(3, 3), tnn.Linear(3, 3), ann.Linear(3, 3))
        net2 = ann.Sequential(ann.Linear(3, 3), tnn.Linear(3, 3), ann.Linear(3, 3))
        self.check_equals(net1, net2)

    def test_can_load_after_forward_pass(self):
        net1 = SimpleNet()
        net2 = SimpleNet()
        x = torch.randn(3)
        net1.forward(x)
        self.save_and_load_at_mock_path(net1, net2)

    def test_can_not_load_if_the_missing_cls_in_state_dict(self):
        net1 = SimpleNet()
        net2 = SimpleNet()
        x = torch.randn(3)
        net2.forward(x)
        with pytest.raises(RuntimeError):
            self.save_and_load_at_mock_path(net1, net2)


class SimpleNet(ann.Module):
    def __init__(self):
        super().__init__()
        self.layer = ann.Linear(3, 3)

    def forward(self, x):
        x = self.layer(x)
        self.cls = distributions.Categorical(logits=x)
        return x
